# Additional steps

Some PDF files are built offline and add to the repo. The exam files are converted from adoc to PDF using:

> asciidoctor-pdf -a pdf-theme=basic -a pdf-themesdir=themes ark2500_h22_mappe_bokmaal.adoc

The course work is converted to PDF using:

>  pandoc ark2500_h22_arbeidskrav_1.md -o ark2500_h22_arbeidskrav_1.pdf

It will be a good idea to standardise this and perhaps generate automatically as part of CI

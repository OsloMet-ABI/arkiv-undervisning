== ARK2510-H22 Arbeidskrav 2

Frist: 2022-10-30

*Bokmål:*

Du må lage filen _arkivstruktur.xml_ der innholdet gjenspeiler strukturen vist 
i figur 1. Filen arkivstruktur.xml skal inneholde følgende Noark 5 
arkivenheter:

* 1 arkiv
* 1 arkivdel
* 1 saksmappe
* 1 journalpost (et innkommende dokument)
* 1 dokumentbeskrivelse
* 1 dokumentobjekt

Merk, arbeidskravet består kun av filen arkivstruktur.xml.

*Nynorsk*:

Du må laga fila _arkivstruktur.xml_ der innhaldet gjenspeglar strukturen vist
i figur i vedlegg 1 . Fila arkivstruktur.xml skal innehalda følgjande Noark 5 
arkivenheter:

* 1 arkiv
* 1 arkivdel
* 1 saksmappe
* 1 journalpost (et innkommende dokument)
* 1 dokumentbeskrivelse
* 1 dokumentobjekt

Merk, arbeidskravet består berre av fila arkivstruktur.xml.

<<<

image:./arkivstruktur.png[arkivstruktur] 

_Figur 1 : arkivstruktur som skal opprettes_

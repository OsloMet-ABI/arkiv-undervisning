

INSERT INTO arkiv (
	systemID, 
	tittel, beskrivelse, 
	arkivstatus, dokumentmedium,
	opprettetDato, opprettetAv) 
VALUES 
 	(
	"b1b6b3f1-9186-4899-b3d9-4ebf0174727c", 
	"Hovedarkivet", null,
	"Opprettet", "Elektronisk arkiv",
	"2021-01-13 08:51:23", "admin"
	);


INSERT INTO arkivdel	(
	systemID, 
	tittel, beskrivelse,
	arkivdelstatus, dokumentmedium, 
	opprettetDato, opprettetAv,
	referanseArkiv
) 
VALUES 
(
	"6cf257fc-262b-4a40-9e00-766d718d8a64", 
	"Opptak bachelor ABI", 	null,
	"Aktiv periode", "Elektronisk arkiv", 
	"2021-01-13 09:01:43", "admin", 
	"b1b6b3f1-9186-4899-b3d9-4ebf0174727c"
);

UPDATE arkivdel SET 
	beskrivelse = "Oppdatert beskrivelse" 
WHERE 
	systemID = "6cf257fc-262b-4a40-9e00-766d718d8a64";


UPDATE arkivdel SET 
	arkivdelstatus = "Avsluttet periode" 
WHERE 
	arkivdelstatus = "Aktiv periode";


DELETE FROM arkiv WHERE 
	systemID = "b1b6b3f1-9186-4899-b3d9-4ebf0174727c";


DELETE FROM arkivdel WHERE 
	systemID = "6cf257fc-262b-4a40-9e00-766d718d8a64";

DELETE FROM arkiv WHERE 
	systemID = "b1b6b3f1-9186-4899-b3d9-4ebf0174727c";


INSERT INTO mappe (systemID, mappeID, tittel, opprettetDato, opprettetAv) VALUES 
 ("4a56e5f7-5ced-4492-b5f3-e75e76d273f0", "2021/1", "Mappe 1", "2021-01-13 08:51:23", "saksbehandler 1"),
 ("2ee3f640-f9ce-4dda-a05b-285a6b28788d", "2021/2", "Mappe 2", "2021-01-13 08:52:03", "saksbehandler 2"),
 ("4e8491f4-65a9-4bf7-b776-cd2dbbe82621", "2021/3", "Mappe 3", "2021-01-13 08:52:43", "saksbehandler 2"),
 ("8d8b449f-9c3b-423c-b39a-77edf7a10975", "2021/4", "Mappe 4", "2021-01-13 08:58:32", "saksbehandler 1");


INSERT INTO registrering (systemID, tittel, opprettetDato, opprettetAv, referanseMappe) VALUES 
 ("18f32487-1e99-4506-928a-eb448adcbcb6", "Registrering 1", "2021-01-13 08:51:23", "saksbehandler 1", "4a56e5f7-5ced-4492-b5f3-e75e76d273f0"),
 ("305f27f7-81df-4a91-bfdf-cdccf2030a33", "Registrering 1", "2021-01-13 08:51:23", "saksbehandler 1", "4a56e5f7-5ced-4492-b5f3-e75e76d273f0"),
 ("55968a3c-62e8-4ab1-b3c0-473bd7c8314d", "Registrering 1", "2021-01-13 08:51:23", "saksbehandler 1", "2ee3f640-f9ce-4dda-a05b-285a6b28788d"),
 ("abab3072-bd8b-4987-b851-6bec28e4e992", "Registrering 1", "2021-01-13 08:51:23", "saksbehandler 1", "2ee3f640-f9ce-4dda-a05b-285a6b28788d"),
 ("75eaf70f-0f54-4612-b388-528aa6ed6eda", "Registrering 2", "2021-01-13 08:52:03", "saksbehandler 2", "4e8491f4-65a9-4bf7-b776-cd2dbbe82621"),
 ("5b6c69d3-a148-4e1f-b686-4bc5e0cfa0e2", "Registrering 3", "2021-01-13 08:52:43", "saksbehandler 2", "4e8491f4-65a9-4bf7-b776-cd2dbbe82621");


SELECT 
  TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME
FROM
  INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE
  REFERENCED_TABLE_SCHEMA = 's123456_noark5' AND
  REFERENCED_TABLE_NAME = 'mappe';


SELECT * FROM mappe, registrering
  WHERE mappe.systemID = 
    registrering.referanseMappe;


# Du kan begrense resultatsettet og spesifisere det maksimale antallet rader du vil ha tilbake
SELECT * FROM mappe LIMIT 3;

# Du kan også feks begrense resultatsett ved å ignorere de første to og ta med de neste 5
SELECT * FROM mappe LIMIT 2,5;

SELECT dokumenttype, COUNT( * ) AS 'Total'
	FROM dokumentbeskrivelse
		GROUP BY dokumenttype;

# Natual join eksempel

SELECT * FROM registrering NATURAL JOIN mappe; 

SELECT * FROM registrering, mappe WHERE mappe.systemID = registrering.referanseMappe;


SELECT * FROM registrering
 LEFT OUTER JOIN 	
  mappe ON
  registrering.referanseMappe = mappe.systemID;





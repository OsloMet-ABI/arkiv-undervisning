CREATE DATABASE s123456_film;
USE s123456_film;

CREATE TABLE Person (
 PersonID INT, 
 Fornavn VARCHAR(20), 
 Etternavn VARCHAR(20), 
 Fodselsdato DATE,
 Dodsdato DATE,
 PRIMARY KEY (PersonID)
) ENGINE = INNODB;

CREATE TABLE Film (
 FilmID INT, 
 Tittel VARCHAR(20), 
 Aar DATE, 
 Lengde SMALLINT, 
 Produsent VARCHAR(20),
 DistributorID INT,
 PRIMARY KEY  (FilmID)
) ENGINE = INNODB;

CREATE TABLE Distributor (
DistributorID INT, 
Navn VARCHAR(20),
Adresse VARCHAR(20),
Postnr VARCHAR(20),
Sted VARCHAR(20),
PRIMARY KEY  (DistributorID)
) ENGINE = INNODB;

CREATE TABLE Kunde (
KundeNR INT, 
Fornavn VARCHAR(20), 
Etternavn VARCHAR(20), 
Adresse VARCHAR(20), 
Postnr VARCHAR(4), 
Sted VARCHAR(20),
PRIMARY KEY  (KundeNR)
) ENGINE = INNODB;

CREATE TABLE Kinosal(
SalNR INT, 
Kapasitet INT, 
FilmID INT,
PRIMARY KEY (SalNR)
) ENGINE = INNODB;

CREATE TABLE Deltagelse(
PersonID INT, 
FilmID INT, 
Rolle VARCHAR(20),
PRIMARY KEY (PersonID, FilmID)
) ENGINE = INNODB;

CREATE TABLE Bestilling(
KundeNR INT, 
FilmID INT, 
Dato DATE, 
Tid TIME, 
Antall INT,
PRIMARY KEY (KundeNR, FilmID)
) ENGINE = INNODB;

# Vi kan også opprette fremmednøkler med ALTER. Fordelen er at vi kan opprette tabellene i vilkårlig rekkefølge
# dersom vi oppretter fremmednøkler når tabellene eksisterer alt.
ALTER TABLE Kinosal ADD FOREIGN KEY (FilmID) REFERENCES Film (FilmID);
ALTER TABLE Bestilling ADD FOREIGN KEY (KundeNR) REFERENCES Kunde (KundeNR);
ALTER TABLE Bestilling ADD FOREIGN KEY (FilmID) REFERENCES Film (FilmID);
ALTER TABLE Deltagelse ADD FOREIGN KEY (PersonID) REFERENCES Person (PersonID);
ALTER TABLE Deltagelse ADD FOREIGN KEY (FilmID) REFERENCES Film (FilmID);

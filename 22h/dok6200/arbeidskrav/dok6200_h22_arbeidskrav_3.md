## DOK6200-H22 Arbeidskrav 3

Frist: 2022-11-19

_Oppdatert: 2022-10-30_

# **Bokmål**

Arbeidskravet består av to spørsmål og du kan velge å besvare enten spørsmål 1 
eller spørsmål 2. 

## Spørsmål 1:

Tabelluttrekk og semantiske uttrekk (feks Noark) er to forskjellige måter å beskrive data fra  
en database for langtidsbevaring. Hva kjennetegner disse tilnærmingene og hvilken hadde du valgt?

Følgende kan inngå i teksten:

* Hva menes med tabelluttrekk?
* Hva menes med semantisk uttrekk?
* Hva kjennetegner tilnæmingene?
* Er standardisert struktur viktig?
* Hvor enkelt er det å finne fram til informasjon?
* Hvor enkelt er det å forstå informasjonen 


## Spørsmål 2:

På side 77 av “Relasjonsdatabser og datamodellering, 3. utgave av Gerd Berget” (figur 1 på denne siden) finner dere noen relasjoner med data fra kino databasen. Arbeidskravet består av å lage XML og XSD som gjør at du kan lagre denne informasjonen på en fornuftig måte for fremtiden. Du må velge om du skal lagre innholdet i hver relasjon som en egen XML fil eller om du synes du bør ha alt i en XML fil eller en kombinasjon av begge strategier.

Arbeidskravet består av følgende leveranser.

1.  Redegjørelse for valgt XML struktur (maks 1 side)
    
2.  Eksempel XML data (maks 3 tupler fra en relasjon)
        
3.  XSD Skjema som kan brukes til å validere XML filen(e)

Hele arbeidskravet skal leveres i én fil. Du kan bruke feks _7-zip_ til å lage denne filen. Det holder med at en av gruppemedlemmene leverer på vegne av hele gruppen, men husk at alle gruppemedlemmer bør identifiseres. Redegjørelsen skal leveres i PDF/A format.

# **Nynorsk**

Arbeidskravet består av to spørsmål og du kan velja å svara på enten spørsmål 1 
eller spørsmål 2. 

## Spørsmål 1:

Tabelluttrekk og semantiske uttrekk (feks Noark) er to forskjellige måtar å beskriva data frå  
ein database for langtidsbevaring. Kva kjenneteiknar desse tilnærmingane og kva hadde du valt?

Følgjande kan inngå i teksten:

* Kva blir meint med tabelluttrekk?
* Kva blir meint med semantisk uttrekk?
* Kva kjenneteiknar tilnæmingene?
* Er standardisert struktur viktig?
* Kvar enkelt er det å finna fram til informasjon?
* Kvar enkelt er det å forstå informasjonen


## Spørsmål 2:


På side 77 av “Relasjonsdatabser og datamodellering, 3. utgåve av Gerd Berget” (figur 1 på denne sidan) finn de nokre relasjonar med data frå kino databasen. Arbeidskravet består av å laga XML og XSD som gjer at du kan lagra denne informasjonen på ein fornuftig måte for framtida. Du må velja om du skal lagra innhaldet i kvar relasjon som ein eigen XML fil eller om du synest du bør ha alt i ein XML fil eller ein kombinasjon av begge strategiar.

Arbeidskravet består av følgjande leveransar

1.  Forklaring for valt XML struktur (maks 1 side)
    
2.  Døme XML data (maks 3 tupler frå ein relasjon)
        
3.  XSD Skjema som kan brukast til å validera XML fila/filene

Heile arbeidskravet skal leverast i éi fil. Du kan bruka feks 7-zip til å laga denne fila. Det held med at eit av gruppemedlemmane leverer på vegne av heile gruppa, men husk at alle gruppemedlemmar bør identifiserast. Forklaringa skal leverast i PDF/A format.


![Kinodatabasen](./figur1.png)
***Figur 1** : Kinodatabasen* 



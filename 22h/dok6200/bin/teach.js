app.controller('CourseController',
    ['$scope', function ($scope) {

        var moduleData = [
                {
                    mid: 1,
                    module_name: "Emnet og diverse",
                    module_description: "Her finnes informasjon om emnet, webinarene og andre opptak som legges ut i løpet av semesteret",
                    module_finish: "2022-11-25",
                    sub_modules: [
                        {
                            sub_module_id: 1,
                            sub_module_name: "Om emnet 2022-08-24",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Oppstartswebinar",
                                    lecture_description: "Overordnet introduksjon og beskrivelse av emnet",
                                    lecture_comment: "",
                                    duration: "131m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=e8655758-a6e5-4799-87aa-aefb00ba20e5"
                                }
                            ]
                        },
                        {
                            sub_module_id: 2,
                            sub_module_name: "Webinarer",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Webinar 1 2022-09-07",
                                    lecture_description: "Komme i gang med emnet, foreslått struktur å ta moduler med tidspunkter",
                                    lecture_comment: "",
                                    duration: "96m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=b7d034b9-9724-40a2-ab74-af110066ff2c"
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Webinar 2 2022-09-21",
                                    lecture_description: "Fra modul 1 til modul 2",
                                    lecture_comment: "",
                                    duration: "97m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=0c74ccec-0bab-4609-a068-af2c00d64b69"
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Webinar 3 2021-10-12",
                                    lecture_description: "Litt om arbeidskrav 2 og oppsummering av Noark og intro til neste modul",
                                    lecture_comment: "",
                                    duration: "102m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=cbb8dc2f-dd7e-479d-90d1-af2c00d2ed53"
                                },
                                {
                                    lid: 4,
                                    lecture_name: "Webinar 4 2021-10-26",
                                    lecture_description: "",
                                    lecture_comment: "",
                                    duration: "100m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=00d56025-ed3e-48d4-98ef-af3b011c6d33"
                                },
                                {
                                    lid: 5,
                                    lecture_name: "Webinar 5 2022-11-09",
                                    lecture_description: "",
                                    lecture_comment: "",
                                    duration: "98m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=871dceb4-b4d5-40a2-96dc-af4900ea9fb1"
                                },
                                {
                                    lid: 6,
                                    lecture_name: "Webinar 6 2022-11-23",
                                    lecture_description: "",
                                    lecture_comment: "Oppsumering",
                                    duration: "96m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=3b08dca9-d97e-4a37-b466-af58014cac9b"
                                }
                            ]
                        },
                        {
                            sub_module_id: 3,
                            sub_module_name: "Spørsmål og svar",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "",
                                    lecture_description: "",
                                    lecture_comment: "",
                                    duration: "",
                                    href: ""
                                }
                            ]
                        }
                    ]
                },
                {
                    mid: 2,
                    module_name: "Databaser og datamodellering",
                    module_description: "Historikken til databaser, ER-modellering, relasjonsmodellen, SQL",
                    module_finish: "2022-09-09",
                    sub_modules: [
                        {
                            sub_module_id: 1,
                            sub_module_name: "Introduksjon",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Historien til databaser",
                                    lecture_description: "En introduksjon som forteller litt om hvor det vi opplever som moderne databaser kommer fra",
                                    lecture_comment: "",
                                    duration: "19m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/f8bf8a894ac344f19d3f8365c9fde55f1d",
                                    slide_href: "foiler/modul1/Kort om historien til databaser.odp"
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Hvorfor bruker vi databaser?",
                                    lecture_description: "Vi bruker databaser fordi de har en del egenskaper som er viktig for å sikre integriteten til data som lagres.",
                                    lecture_comment: "",
                                    duration: "34m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/08cba3ab7c6e4bdb90db89be38b141b21d",
                                    slide_href: "foiler/modul1/Hvorfor bruker vi databaser.odp"
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Hva er en database?",
                                    lecture_description: "En database er en strukturert / organisert samling av data lagret (elektronisk) og gjort tilgjengelig via et datasystem",
                                    lecture_comment: "",
                                    duration: "6m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/1c53aef42fcd441fa9643aba111fdbc91d",
                                    slide_href: ""
                                },
                                {
                                    lid: 4,
                                    lecture_name: "Relasjonsmodellen",
                                    lecture_description: "Relasjonsmodellen er en modell som mange moderne DBHS er implemtert ettet. Her se vi litt på modellen og noe av terminioligien som er viktig å ha med seg videre i emnet.",
                                    lecture_comment: "",
                                    duration: "20m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/31d9a456a7424e3baa1a795212e10a211d",
                                    slide_href: ""
                                },
                                {
                                    lid: 5,
                                    lecture_name: "Tilgang til MySQL",
                                    lecture_description: "Hvordan du får tilgang til MySQL serveren som vi jobber med",
                                    lecture_comment: 'MySQL via phpmyadmin (https://bibin.oslomet.no/phpmyadmin/), Opprett konto (https://bibin.oslomet.no/sql/). Merk du må være student for å få tilgang',
                                    duration: "7m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/09866ad512654749a436292de7017b211d",
                                    slide_href: ""
                                }
                            ]
                        },
                        {
                            sub_module_id: 2,
                            sub_module_name: "Structured Query Language (SQL)",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Introduksjon",
                                    lecture_description: "Hva er SQL og hvordan brukes det. Vi tar også en gjennomgang av phpmyadmin og ser litt på redigeringsprogrammer som vi bruker for å utvikle SQL spørringer",
                                    lecture_comment: "Windows: Bruk notepad++. Mac: Netbeans, atom. Linux: gedit, netbeans, atom",
                                    duration: "23m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/51b101a975514906a41c7ec933e46fe41d",
                                    slide_href: ""
                                },
                                {
                                    lid: 2,
                                    lecture_name: "SQL synktaks og bruk 1",
                                    lecture_description: "Grunnleggende SQL og litt om filtrering med WHERE og bruken av jokertegn",
                                    lecture_comment: "",
                                    duration: "28m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/0ab6aaed8d0d43918537b4357daa991a1d",
                                    slide_href: ""
                                },
                                {
                                    lid: 3,
                                    lecture_name: "SQL synktaks og bruk 2",
                                    lecture_description: "Mer om filtrering med LIMIT, DISTINCT, ORDER BY og GROUP BY",
                                    lecture_comment: "",
                                    duration: "28m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/0125632cdc3d4c8d88ac86cde8d25dc01d",
                                    slide_href: ""
                                },
                                {
                                    lid: 4,
                                    lecture_name: "Joins",
                                    lecture_description: "Databasetabeller inneholder ofte informasjon som er koblet sammen med fremmednøkler. Her ser vi hvordan vi kan jobbe med sammenkoblet data",
                                    lecture_comment: "",
                                    duration: "25m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/4436fa86cbdd459282375470c4d7936a1d",
                                    slide_href: ""
                                },
                                {
                                    lid: 5,
                                    lecture_name: "SQL visninger",
                                    lecture_description: "En SQL visning er en virtuell tabell som baseres på resultat av en SQL spørring",
                                    lecture_comment: "",
                                    duration: "4m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/e2db424620f043bbb470e2f646b24d3f1d",
                                    slide_href: ""
                                }
                            ]
                        },
                        {
                            sub_module_id: 3,
                            sub_module_name: "ER-modellering og normalisering",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "ER-modellering",
                                    lecture_description: "Modellering er et viktig steg i prossesen med å lage en database",
                                    lecture_comment: "",
                                    duration: "50m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/a3febe81a5254e5cba0cb7efa8b1c5721d",
                                    slide_href: ""
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Normalisering",
                                    lecture_description: "Hva er normalisering og hvorfor er det viktig?",
                                    lecture_comment: "",
                                    duration: "58m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/7dd2ebf03e45410fb6f8909c39deedb81d",
                                    slide_href: ""
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Logisk skjema",
                                    lecture_description: "Utvikling av logisk skjema er siste steg for å bygge databasestrukturen",
                                    lecture_comment: "",
                                    duration: "12m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/fe4287f11e314a96942df393c3a76c9c1d",
                                    slide_href: ""
                                }
                            ]
                        },
                        {
                            sub_module_id: 4,
                            sub_module_name: "DDL og DML",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Datatyper brukt i databaser",
                                    lecture_description: "Attributter må ha en datatype definert når tabellen opprettes",
                                    lecture_comment: "",
                                    duration: "27m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/c54bd137dd1a4a52bf2668703bb1ac8d1d",
                                    slide_href: ""
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Data Definition Language (DDL)",
                                    lecture_description: "DDL er verktøyet vi bruker for å opprette, slette og endre database struktur",
                                    lecture_comment: "",
                                    duration: "49m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/9a2c210e5cfe4d899797bb432f0a22ec1d",
                                    slide_href: ""
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Data Manipulation Language (DML)",
                                    lecture_description: "DML brukes til å sette inn, endre og slette innhold i en tabell",
                                    lecture_comment: "",
                                    duration: "43m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/51b6766089d249839dc13fab54b4d31f1d",
                                    slide_href: ""
                                }
                            ]
                        },
                        {
                            sub_module_id: 4,
                            sub_module_name: "Oppsummering",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Modul 1 oppsummert",
                                    lecture_description: "En oppsummring av det somer gjennomgått i modul 1",
                                    lecture_comment: "",
                                    duration: "6m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/96c76648503c46c78a553aa4df00dde51d"
                                }
                            ]
                        }
                    ]
                },
                {
                    mid: 3,
                    module_name: "Dokumentasjonsforvaltning",
                    module_description: "Implementasjon av Noark i database og diskuterer diverse perspektiver rundt dette.",
                    module_finish: "2022-09-23",
                    sub_modules: [
                        {
                            sub_module_id: 1,
                            sub_module_name: "Introduksjon",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Introduksjon",
                                    lecture_description: "Modulen bygger på det som er gjennomgått i modul 1 og lar oss ta i bruke det vi lærte i praksis",
                                    lecture_comment: "",
                                    duration: "3m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/e4938761258942de974c6db31f68fa7f1d",
                                    slide_href: ""
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Intro til Noark og dokumentasjonsforvaltning",
                                    lecture_description: "Noark er en standard som kan brukes som en 'best-practice' for  dokumentasjonsforvaltning. Her begynner vi med en fugleperspektiv på hva Noark 5 er og hvordan det kan være en veileder for  dokumentasjonsforvaltning",
                                    lecture_comment: "",
                                    duration: "50m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/8d149bddef79438bb1e5a233bb5a49b01d",
                                    slide_href: ""
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Intro til arkivstruktur",
                                    lecture_description: "Datamodellen til Noark (arkivstruktur) er en av de viktigste delene av Noark",
                                    lecture_comment: "",
                                    duration: "50m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/5191330f119b494f8e4858dfaa8d15461d",
                                    slide_href: ""
                                }
                            ]
                        },
                        {
                            sub_module_id: 2,
                            sub_module_name: "Noark 5 DDL",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Arkiv og arkivdel",
                                    lecture_description: "Metadata til arkiv/arkivdel og hvordan de kobles med en fremmednøkkel. Litt diskusjon rundt hvorfor det kanskje ikke er relevant for et fagsystem. Litt om hvordan moderne samhandlingsplatformer er Noark vennlig.",
                                    lecture_comment: "",
                                    duration: "71m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/7c1cf4225a514924ade6b30959e9707d1d",
                                    slide_href: ""
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Klassifikasjonssytem og klasse",
                                    lecture_description: "Metadata til klassifikasjonssytem og klasse og hvordan de kobles med en fremmednøkkel. Litt mer om interne fremmednøkler",
                                    lecture_comment: "",
                                    duration: "32m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/cf5acce553d842069064d13a0a79f0f81d",
                                    slide_href: ""
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Mappe og registrering",
                                    lecture_description: "Metadata til mappe og registrering og hvordan de utgjør en transaksjonskontekst. Mappe og registrering kan utvides til spesialisering (arv)",
                                    lecture_comment: "",
                                    duration: "44m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/51f4f2094e5e49529cd162666ad44bc31d",
                                    slide_href: ""
                                },
                                {
                                    lid: 4,
                                    lecture_name: "Dokumentbeskrivelse og dokumentobjekt",
                                    lecture_description: "Metadata til dokumentbeskrivelse og dokumentobjekt og hva en dokumentkontekst er og hvorfor det er viktig for elektronisk materiale. En forklaring på hva en sjekksumm er. En liten oppsummering av arkivstruktur",
                                    lecture_comment: "",
                                    duration: "50m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/a3998714f51747448859f29fce68f72c1d",
                                    slide_href: ""
                                }
                            ]
                        },
                        {
                            sub_module_id: 3,
                            sub_module_name: "Noark 5 DDL",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Noark 5 og DDL",
                                    lecture_description: "Hvordan sette Noark data inn i en tabell. Litt om forksjellen mellom rollene bruker, system og database her på å angi verdier",
                                    lecture_comment: "",
                                    duration: "38m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/d4c79d59950644858b5cd0fd2fce44981d",
                                    slide_href: ""
                                }
                            ]
                        },
                        {
                            sub_module_id: 4,
                            sub_module_name: "Noark 5 SQL",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Noark 5 og SQL",
                                    lecture_description: "Overordnet gjennomgang av SQL søk med Noark og hva som er interessant å kunne hente ut. Spesielt fokus på joins da disse har en viktig rolle når uttrekk lages. Inkluderer også perspektiver rundt det å jobbe i en database ut ifra en Noark kontekst.",
                                    lecture_comment: "",
                                    duration: "44m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/b8f26cc181ee45c9b327a91e4e46200f1d",
                                    slide_href: ""
                                }
                            ]
                        }
                    ]
                },
                {
                    mid: 4,
                    module_name: "Perspektiver",
                    module_description: "Her går vi litt bredere og ser mer på litt teori rundt praksis",
                    module_finish: "2022-10-15",
                    sub_modules: [
                        {
                            sub_module_id: 1,
                            sub_module_name: "Introduksjon",
                            lectures: [{
                                lid: 1,
                                lecture_name: "Introduksjon",
                                lecture_description: "4 artikler legges vekt på for å danne en teroretisk kontekst for emnet",
                                lecture_comment: "",
                                duration: "7m",
                                href: "https://mediasite.oslomet.no/Mediasite/Play/018178f44495417bac8a5db87ad7b3101d"
                            }
                            ]
                        }, {
                            sub_module_id: 2,
                            sub_module_name: "Hurley. What if anything ...",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Hva er er dokumentasjonsforvaltning?",
                                    lecture_description: "Hurley stiller spørsmål til hva dokumentasjonsforvaltning er og hvordan det påvirkes av endringer.",
                                    lecture_comment: "",
                                    duration: "38m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/c19295e139dd462fa5334753f01cffe11d"
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Utviklingen som skapte en usikker tid",
                                    lecture_description: "Hva skjedde når 'ansatte' ble 'brukere'?. Litt mer om dokumentasjon.",
                                    lecture_comment: "",
                                    duration: "50m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/882085a11bbf45ecbe08276d257067e91d"
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Fra digitalisering til rekonstruksjon",
                                    lecture_description: "Digitalisering eller automatisering, er ikke målet. Evnen til rekonstruksjon er det som er viktig.",
                                    lecture_comment: "",
                                    duration: "23m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/a59373b772894a188669b85a693e2db11d"
                                },
                                {
                                    lid: 4,
                                    lecture_name: "Forretnings- og dokumentasjonsprosesser",
                                    lecture_description: "Forholdet mellom forretnings- og dokumentasjonsprosesser. Hva tilbyr Noark i et slikt perspektiv.",
                                    lecture_comment: "",
                                    duration: "38m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/edb11f267ebf40dcab2ce7145f1a4f911d"
                                },
                                {
                                    lid: 5,
                                    lecture_name: "Hurley i en norsk kontekst",
                                    lecture_description: "Litt om det Hurley ikke kun si så mye om i 2004. Personvern, nettsky osv. Hvilken perspektiv er egentlig det som er vikitg? Bevis, bevaring, prosses eller søk?",
                                    lecture_comment: "",
                                    duration: "40m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/6ef1aad0ecdd4209a26081af17bb5cc81d"
                                }


                            ]
                        },
                        {
                            sub_module_id: 3,
                            sub_module_name: "Duranti. Concepts, principles ...",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Utfordringer med dokumentasjonsforvaltning",
                                    lecture_description: "Hvorfor dokumentasjonsforvaltning vanskelig?",
                                    lecture_comment: "",
                                    duration: "54m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/0b9d10984b174b36b6eaa32a6f953f9b1d"
                                }, {
                                    lid: 2,
                                    lecture_name: "Sikre pålitlighet og autentisitet",
                                    lecture_description: "Mer fokus på autentisitet. En bredere definasjon av egenskapene til et arkivdokument som er kanskje bredere en det vi er vant til. Signraturer og andre mekanismer som sikrer pålitlighet.",
                                    lecture_comment: "",
                                    duration: "64m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/916fabb3ba4a402bb38ff6ca082181101d"
                                }
                            ]
                        },
                        {
                            sub_module_id: 4,
                            sub_module_name: "McDonald. RM/DM closing the gap ",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Dokumentasjonsforvaltning og dataledelse",
                                    lecture_description: "To profesjoner som forvalter data kniver om retten til å bestemme. Er det bare en som kan vinne?",
                                    lecture_comment: "",
                                    duration: "46m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/2239d30cab6241cabc7fecc9a34151ef1d"
                                }
                            ]
                        },
                        {
                            sub_module_id: 5,
                            sub_module_name: "Sødring IoT",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "IoT utfordringer",
                                    lecture_description: "Hva er IoT og hvorfor kan det være problematisk for dokumentasjonsforvaltning?",
                                    lecture_comment: "",
                                    duration: "32m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/85326e4c8a40437c8578299abbbdf1201d"
                                }, {
                                    lid: 2,
                                    lecture_name: "GARP prinsippene og case",
                                    lecture_description: "Her presenterer vi caset som artikkelen baserer seg på. Deretter diskuterer vi GARP-prinsippene og tar de i bruk for caset.",
                                    lecture_comment: "",
                                    duration: "40m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/de2eb3a737b34c89b9cbba4b0c022e901d"
                                }, {
                                    lid: 3,
                                    lecture_name: "GARP i Norge?",
                                    lecture_description: "Kan Noark brukes i begge casene? Hvilken prinsipper bruker vi i Norge? Danning og bevaring er to sider av samme sak. Ser vi en helhet eller bar stykkvis",
                                    lecture_comment: "",
                                    duration: "49m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/fd12ccb5b07b4ff1ace8605aac259df11d"
                                }
                            ]
                        }
                    ]
                },
                {
                    mid: 5,
                    module_name: "Bevaring av dokumentasjon",
                    module_description: "Hvordan bevarer vi dokumentasjon? Hvilken formater bruker vi og hvordan kan vi migrere data for bevaring",
                    module_finish: "2022-11-05",
                    sub_modules: [
                        {
                            sub_module_id: 1,
                            sub_module_name: "Introduksjon",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Introduksjon",
                                    lecture_description: "I modulen vil vi gå gjennom XML og XSD og samtidig sette det i et arkivfaglig kontekst.",
                                    lecture_comment: "",
                                    duration: "2m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/39d2e199ab7346deb746fa049b66bff11d"
                                }
                            ]
                        },
                        {
                            sub_module_id: 2,
                            sub_module_name: "XML",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Introduksjon til XML",
                                    lecture_description: "Hva er et markeringspråk, hvor kommder det fra og hvorfor er det viktig i et arkivsammenheng? Litt forskjelige tilnærminger til å overføre data med vekt på XML",
                                    lecture_comment: "",
                                    duration: "51m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/502604a0d5794895af8966529d43c79f1d"
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Oppbygging av et XML dokument",
                                    lecture_description: "Et XML dokument består av prolog og rotelement/dokumentelement. Litt om verktøy som kan brukes og vår første XML-dokument",
                                    lecture_comment: "",
                                    duration: "22m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/1f50eb841c2849ffb981dbec29310fbd1d"
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Utvikling av XML dokumenter",
                                    lecture_description: "Fortsetter med flere eksempler og gradvis bygger ut hva vi beskrive i XML dokumentet. Litt om hvordan du må samhandle med verktøyet.",
                                    lecture_comment: "",
                                    duration: "33m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/1ed04a5056884bb9acb8b405fba1def31d"
                                },
                                {
                                    lid: 4,
                                    lecture_name: "XML videre",
                                    lecture_description: "Utvikler et XML-dokument som kan beskrive en bok. Ser litt på hvordan struktur blir viktig. Tar opp forholdet XML/database der det viktig å definere et element som avgrenser en rad av informasjon i en database tabell",
                                    lecture_comment: "Struktur og validering er noe som kommer i modulen om XSD",
                                    duration: "22m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/6c8d2535fb344b8095e7eb6604e089b71d"
                                },
                                {
                                    lid: 5,
                                    lecture_name: "XML-'stacken'",
                                    lecture_description: "Du må se på XML i forhold til helheten det tilbyr. Ser litt på xpath og xslt.",
                                    lecture_comment: "Dette er ikke noe det forventes at dere skal lære, eller kunne gjøre selv. Det vises for å gi et inntrykk i hva XML-stacken kan gjøre.",
                                    duration: "26m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/08f7901c405448269330a8aee6a47bc61d"
                                }
                            ]
                        },

                        {
                            sub_module_id: 3,
                            sub_module_name: "XSD",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Introduksjon til XSD",
                                    lecture_description: "Hvorfor er det viktig å definere strutur til XML dokumenter? Innom dette med korrekt strukturet og gyldighet.",
                                    lecture_comment: "",
                                    duration: "31m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/8975169cffd24104afb5c9252bcd73a41d"
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Korrekt strukturert",
                                    lecture_description: "Hvilken regler gjelder for oppbygging og syntaks av et XML-dokument",
                                    lecture_comment: "",
                                    duration: "41m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/4b05d85236db4c8aaa88a544671b74441d"
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Litt mer om XSD",
                                    lecture_description: "Hva XSD kan gjøre og litt om interoperabilitet. Datatyper brukt i XSD",
                                    lecture_comment: "",
                                    duration: "23m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/134440d450ed45ae920889420cdedfd11d"
                                },
                                {
                                    lid: 4,
                                    lecture_name: "Utvikle XSD og test  av gyldiget",
                                    lecture_description: "Hvordan du bygger opp en XSD-fil. Hvordan kobles XML/XSD sammen for validering",
                                    lecture_comment: "",
                                    duration: "34m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/78a0bc04b17f479abb7e32eb20ff55741d"
                                },
                                {
                                    lid: 5,
                                    lecture_name: "Struktur og forekomster",
                                    lecture_description: "Struktur defineres med bruken av complexType, mens min/maxOccurs brukes til å si hvor mange ganger et element kan forekomme",
                                    lecture_comment: "",
                                    duration: "51m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/035d3c5af6f349cdbb1e5db38b184ae61d"
                                },
                                {
                                    lid: 6,
                                    lecture_name: "XSD restriksjoner",
                                    lecture_description: "XSD restriksjoner kan si noe om gyldigheten til innholdet i XML-dokumentet. Tidligere har vi sett på gyldighet i forhold til struktur og forekomst av elementer. Her blir fokus på gydlighet til innhold.",
                                    lecture_comment: "",
                                    duration: "42m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/e66b619ef4364020aef72833bf179cbf1d"
                                },
                                {
                                    lid: 7,
                                    lecture_name: "Andre måter å bygge XSD",
                                    lecture_description: "Vi har lært en russian doll tilnærming for å lage XSD. Det finnes andre måter og Noark bruke en av disse andre tilnærmingene.",
                                    lecture_comment: "Ikke noe alle trenger å vite, men gir en forståelse av hvorfor Arkivverket ikke bruker Russian doll tilnærmingen.",
                                    duration: "14m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/9fe8c86b3d694e15a11220f11089cc2a1d"
                                },
                                {
                                    lid: 8,
                                    lecture_name: "Hvordan generere XSD fra XML",
                                    lecture_description: "Dersom du har et XML dokument kan du genrere en XSD for å få oversikt over strukturen.",
                                    lecture_comment: "",
                                    duration: "10m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/11f5a7594bef428d85895b8c5da4183f1d"
                                },
                                {
                                    lid: 9,
                                    lecture_name: "Oppsummering",
                                    lecture_description: "Dersom du har et XML dokument kan du genrere en XSD for å få oversikt over strukturen.",
                                    lecture_comment: "",
                                    duration: "10m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/e7e0f06087444a9da7ad9644e1c9c9681d"
                                }
                            ]
                        }
                    ]
                },
                {
                    mid: 6,
                    module_name: "Bevaring av databaser",
                    module_description: "Databaser kan lagres i formater som ADDML eller SIARD. Her ser vi på SIARD og hvordan det kan brukes til bevaring.",
                    module_finish: "2022-11-25",
                    sub_modules: [
                        {
                            sub_module_id: 1,
                            sub_module_name: "Introduksjon",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Bevaringstrategier",
                                    lecture_description: "En introduksjon til modulen der vi tar utgangspunkt i forskjellig bevaringstrategier og lander på den kontroversielle strategien migrering.",
                                    lecture_comment: "",
                                    duration: "30m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/60f0b79c038b45dbb1bc2b9ce1cbe0071d"
                                }, {
                                    lid: 2,
                                    lecture_name: "Opprettholde forståelighet",
                                    lecture_description: "Det kan være vanskelig å opprettholde forståeligheten til dokumentasjon når det migreres.",
                                    lecture_comment: "",
                                    duration: "35m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/91a755fb608a4a2bb1dd625694936c1d1d"
                                }, {
                                    lid: 3,
                                    lecture_name: "Semantiske eller tabelluttrekk",
                                    lecture_description: "To tilnaærminger til uttrekk fra database finnes. Semantiske (feks Noark) eller tabelluttrekk (feks SIARD). Vi ser på forskjellen og litt hvordan SIARD kan løftes til en forstå nivå",
                                    lecture_comment: "",
                                    duration: "56m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/97fdb6ef9c224cc09b58335c123375c21d"
                                }
                            ]
                        },
                        {
                            sub_module_id: 2,
                            sub_module_name: "Noark 5 uttrekk",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Noark som OAIS pakke",
                                    lecture_description: "OAIS standarden definerer egenskapene til en arkivpakke. Her blir de gjennomgått. ",
                                    lecture_comment: "Merk. Opptaket skjedde på feil skjerm!",
                                    duration: "46m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/7005e446644442549a57a460dfdfef1a1d"
                                }, {
                                    lid: 2,
                                    lecture_name: "Overordnet beskrivelse",
                                    lecture_description: "En beskrivelse av de overodnet kravene til et Noark 5 uttrekk",
                                    lecture_comment: "Merk. Opptaket skjedde på feil skjerm!",
                                    duration: "52m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/18820ea806114311a609822c8d4f6b151d"
                                }, {
                                    lid: 3,
                                    lecture_name: "arkivuttrekk.xml",
                                    lecture_description: "arkivuttrekk inneholder mye proveniensinformasjon og er en overordnet beskrivelse av innhold på et 'meta'-nivå.",
                                    lecture_comment: "I opptaket står det at info.xml også blir gjennomgått. info.xml blir håndtert i egen video.",
                                    duration: "50m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/64960e6c93c94290a63e731cb53ef8881d"
                                }, {
                                    lid: 4,
                                    lecture_name: "arkivstruktur.xml - intro",
                                    lecture_description: "Hvilken strukturer kan være i arkivstruktur.xml og litt om bruken. Overorndet intro.",
                                    lecture_comment: "",
                                    duration: "36m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/d778f3c21a2a4a0fb017da098977a9691d"
                                }, {
                                    lid: 5,
                                    lecture_name: "arkivstruktur.xml - XSD og arkiv",
                                    lecture_description: "Vi ser hvordan arkivstruktur.xsd brukes og jobber med å utvikle arkivstruktur. Tar arkiv arkivenheten.",
                                    lecture_comment: "",
                                    duration: "43m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/a12fc963122a49a1bda965b51b28abba1d"
                                }, {
                                    lid: 6,
                                    lecture_name: "arkivstruktur.xml - arkivskaper, arkivdel, klassifikasjonsystem og klasse",
                                    lecture_description: "Fortsetter utvikling av arkivstruktur med overnevnte arkivenheter",
                                    lecture_comment: "",
                                    duration: "39",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/2a2a7dd253154a8d8ce5660eba3e0bf11d"
                                }, {
                                    lid: 7,
                                    lecture_name: "arkivstruktur.xml - mappe og registrering",
                                    lecture_description: "Fortsetter utvikling av arkivstruktur med overnevnte arkivenheter. Vi ser også på hvordan mappe utvides til saksmappe og regsitrering til journalpost. Vi gjør første validering av XML mot XSD.",
                                    lecture_comment: "",
                                    duration: "47m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/1662836b24f94fb3a82076b5f42466fd1d"
                                }, {
                                    lid: 8,
                                    lecture_name: "arkivstruktur.xml - dokumentbeskrivelse og dokumentobjekt",
                                    lecture_description: "Kommer til slutten av arkivstruktur med dokumentkonteksten",
                                    lecture_comment: "",
                                    duration: "33m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/b53c87358e234a959368665b474221df1d"
                                }, {
                                    lid: 9,
                                    lecture_name: "endringslogg.xml - oversikt",
                                    lecture_description: "endringsloggen inneholder historikken/utviklingen til bestemte elemenet som finnes i arkivstrukturen. Det kan anses som proveniensinformasjon, men kan også være en del av autentisiteten.",
                                    lecture_comment: "",
                                    duration: "22m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/999a3f329f264382913ba5218c2f108a1d"
                                }, {
                                    lid: 10,
                                    lecture_name: "journalrapportene",
                                    lecture_description: "offentlig- og løpendejournal er en slags visning over innholdet i arkivstruktur der fokuset er på journalposten framfor arkivstruktur.",
                                    lecture_comment: "",
                                    duration: "22m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/7b24d371313e48629d847b1f829d0f951d"
                                }, {
                                    lid: 11,
                                    lecture_name: "virksomhetsspesifikkeMetadata",
                                    lecture_description: "Merk: Dette er ikke så relevant for eksamen. Bruken av virksomhetsspesifikkeMetadata gjør det mulig å ta i bruk metadata som ikke er en del av arkivstruktur. Det er et veldig nyttig verktøy for dokumentasjonsforvaltning.",
                                    lecture_comment: "",
                                    duration: "19m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/34c5208229ca4de29ee91b187d53fb081d"
                                }, {
                                    lid: 12,
                                    lecture_name: "arkivdokumenter",
                                    lecture_description: "Litt om arkivdokumenter som inngår i uttrekket.",
                                    lecture_comment: "",
                                    duration: "20m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/e37d63fb9a344c73894bf87ee7e7f8001d"
                                }, {
                                    lid: 13,
                                    lecture_name: "info.xml",
                                    lecture_description: "info.xml er siste instans av et interessant tilnærming til hvordan autentisitet blir håndtert.",
                                    lecture_comment: "",
                                    duration: "25m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/1acf37e669ec47cc8d033fbf72a0130b1d"
                                }, {
                                    lid: 14,
                                    lecture_name: "Validering av uttrekk",
                                    lecture_description: "Oppsummerer et Noark 5 uttrekk",
                                    lecture_comment: "",
                                    duration: "15m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/48454ffbc95c483291864a946deb096b1d"
                                }, {
                                    lid: 15,
                                    lecture_name: "Oppsumering",
                                    lecture_description: "Oppsummerering av Noark 5 uttrekk",
                                    lecture_comment: "",
                                    duration: "29m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/7cea92206d624001b46b03894b3cbc7c1d"
                                }
                            ]
                        },
                        {
                            sub_module_id: 3,
                            sub_module_name: "Tabelluttrekk",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Bevaring av dokumentasjon",
                                    lecture_description: "Hvor enkelt er det å bevare dokumentasjon, spesielt når det er lagret som tabeller. Vi møter noe av det vi har sett på tidligere og bruker det som et utgangspunkt for tabelluttrekk",
                                    lecture_comment: "",
                                    duration: "42m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/6a2fb752dc9a480981eaa29fbfe86e8e1d"
                                }, {
                                    lid: 2,
                                    lecture_name: "Hvordan migreres data til XML",
                                    lecture_description: "Her ser vi hvordan vi rent praktisk kan trekke data fra en tabell og markere det som XML. Vi ser på to tilnærminger, tabell og semantisk",
                                    lecture_comment: "",
                                    duration: "17m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/b9dcc90e3bd74dcebf9ae5ce61c83afa1d"
                                }, {
                                    lid: 3,
                                    lecture_name: "Hvordan beskrive database egenskaper ",
                                    lecture_description: "Databasegenskaper som feks primær- og fremmednøkler må beskrives med tabelluttrekk. Her ser vi en enkel måte å gjøre det. Vi oppsummerer denne strategien her også",
                                    lecture_comment: "",
                                    duration: "16m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/e3f696e684074977bd80fd9df61ef6651d"
                                }, {
                                    lid: 4,
                                    lecture_name: "Siard Intro",
                                    lecture_description: "Siard er en format for å lagre data i en relasjonstabell som XML. Det er nyttig at det finnes en standard som gjør dette, men det er mye verdi at SIARD prøver å gjøre uttrekkene uavhengig av databaseleverandører",
                                    lecture_comment: "",
                                    duration: "20m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/6f877d9aceec49f89590f547328a23b81d"
                                }, {
                                    lid: 5,
                                    lecture_name: "Fra database til Siard fil",
                                    lecture_description: "Hvordan dbtk brukes for å trekke database ut fra en database. Hva kan dbtk tilby av innsyn",
                                    lecture_comment: "",
                                    duration: "24m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/271a04e8edc74db28e66c97dcfd4b3e31d"
                                }, {
                                    lid: 6,
                                    lecture_name: "Fra database til Siard fil ... igjen",
                                    lecture_description: "Samme innhold som Fra database til Siard fil opptaket, bare at vi her går gjennom prossesen på ektevis",
                                    lecture_comment: "",
                                    duration: "13m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/52d1980046ec48eaaf3ebb65a80274c71d"
                                }, {
                                    lid: 7,
                                    lecture_name: "Fra Siard til database",
                                    lecture_description: "Her ser vi hvordan vi kan eksportere data fra Siard filen tilbake til en database",
                                    lecture_comment: "",
                                    duration: "10m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/760909aaf5e44fabbb3636af7d0f5c9c1d"
                                }, {
                                    lid: 8,
                                    lecture_name: "Innhold i Siard filen",
                                    lecture_description: "Vi pakker ut siard filen og ser på innholdet, hvordan innhold og databaseteknisk metadata er lagret",
                                    lecture_comment: "",
                                    duration: "18m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/89bae452359c40b28c69fef691f370711d"
                                }, {
                                    lid: 9,
                                    lecture_name: "Oppsummering Siard",
                                    lecture_description: "Oppsumering av det som er gjennomgått",
                                    lecture_comment: "",
                                    duration: "19m",
                                    href: "https://mediasite.oslomet.no/Mediasite/Play/1b3ffdeae77e476cb3746232a27f8f251d"
                                }
                            ]
                        }
                    ]
                }
            ]
        ;

        $scope.modules = moduleData;

        $scope.doShowModuleContents = function (module) {
            $scope.currentModule = module;
            $scope.currentLecture = module.sub_modules[0].lectures[0];
            console.log(JSON.stringify(module));
            $('#courseModalDir').modal('show');
        };

        $scope.doShowLectureContents = function (lecture) {
            $scope.currentLecture = lecture;
            console.log(JSON.stringify(lecture));
        };
    }])
;




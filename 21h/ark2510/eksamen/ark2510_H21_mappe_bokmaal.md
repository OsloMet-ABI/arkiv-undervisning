## ARK2200-H21 - Digital dokumentasjonsforvaltning

**Mappeeksamen**

Vær oppmerksom på at:

- Eksamen består av fire deler og alle deler må besvares.
- Når antall tegn er oppgitt inkluderer det ikke eventuell litteraturliste eller innholdsfortegnelse. Det inkluderer mellomrom.
- Wikipedia er ikke en god referansekilde! Unngå å bruke wikipedia som referansekilde. Wikipedia vil ofte referere til en god referansekilde, bruk disse referansene i stedet.
 

## Spørsmål 1: (9 800 tegn – 14 300 tegn)_

_I boken «1984» skrev George Orwell «Den som kontrollerer fortiden styrer fremtiden. Den som styrer nåtiden, kontrollerer fortiden». Arkivene inneholder samfunnets historikk, men kan vi være sikker på at det som er i arkivene er autentisk? Diskuter denne problemstillingen i lys av hvordan autentisitet praktiseres på et elektronisk arkivdokumentnivå i Norge._

## Spørsmål 2: 

### Del 1.**

Du må lage et Noark 5 uttrekk med en struktur som er i samsvar med strukturen vist i vedlegg 1. Uttrekket må oppfylle alle kravene til et Noark 5 uttrekk slik de er definert i kapittel 6.4 av Noark 5-standarden (Noark 5 v 5).

Løsningen må inneholde følgende filer som du skal utvikle:

- arkivstruktur.xml 
- arkivutrekk.xml
- offentligjournal.xml
- loependejournal.xml
- endringslogg.xml
- info.xml

Gjeldende XSD-filer lastes ned og inkluderes i uttrekket.

En mappe med navnet _dokumenter_ må være med i uttrekket og alle dokumenter i denne katalogen må være i et arkivformat. Alle XML-filer må være velformede og gyldige. All data skal ha fornuftige verdier. Arkade5 kan brukes til å teste uttrekket ditt.

Filen arkivstruktur.xml må inneholde følgende:

- 1 arkiv
- 2 arkivskaper
- 1 arkivdel
- 1 klassifikasjonsystem
- 1 klasse
- 1 saksmappe
- 1 journalpost per saksmappe
- 1 arkivnotat per saksmappe
- 1 korrespondansePart per journalpost / arkivnotat
- 1 dokumentbeskrivelse per journalpost / arkivnotat
- 1 dokumentobjekt per dokumentbeskrivelse (skal være i arkivformat)

Filen endringslogg.xml trenger bare å inneholde to <endring> elementer med tilhørende data. Du skal vise endringer på <tittel> elementet for en journalpost og for en arkivnotat.

Filene lopendejournal.xml og offentligjournal.xml skal stemme overens med arkivstruktur.xml. Filen info.xml skal også være i samme mappe som uttrekket.

**Tilleggsinformasjon**. Dette er et rent elektronisk arkiv så M301 oppbevaringssted skal ikke være med. referanseArkivdel kan ansees å være 0-1, ikke 0-M for mappe og registrering. virksomhetsspesifikkeMetadata er ikke relevant i dette eksempelet. Navn på elementer må følge Noark 5 metadatakatalogen. Metadatakatalogen kan være nyttig til å forstå metadataelementene for å komme fram til fornuftige verdier. Noark 5 v 5 skal brukes som grunnlag for oppgaven.


### Del 2. _(2300 – 3400 tegn)_

Redegjør for hvordan et Noark 5 uttrekk utgjør et OAIS Submission Information Package.

## Spørsmål 3: 
 

_**Spørsmål 4:**_ _Filformater_ _(9 800 tegn – 14 300 tegn)_

_Kommunen_ _du jobber for_ _har_ _funnet noe_ _eldre_ _digital_ _og analog_ _materiale som_ _vurderes_ _bevar__t__._ _Det viser seg at dette materialet stammer fra_ _et_ _innbyggerinitiativ på_ _slutten av_ _9__0__-tallet der_ _turis__tforeningen i kommunen har gitt råd til kommunen om hvordan kommunen bør utforme sin friluftsstrategi._ _Samlingen_ _inneholder_ _60_ _dokumenter_ _i .__lwp_ _filformate__t__,_ _1__5 regneark utviklet i_ _Lotus_ _Notes_ _og 20 CD-ROM, uten omslag._ _I tilleg er det funnet 150 filer uten filendelse men en analyse viser at de første 4 tegnene i filene begynner med Hex verdiene «__FF 57 50 43__»._ _Dessverre_ _finnes_ _det_ _ingen tilhørende_ _uavhengig_ _metadata._ _Det er bestemt at materialet skal bevares, men kommunen er usikker hvordan de skal gå fram._ _Lag en rapport som_ _beskriver_ _hvordan kommunen_ _bør_ _forsøke å bevare materialet._

 
 

 
 

 
 

 
 

 
 

_**Vedlegg 1:**_

 
 

 
 

 
 

 
 

 
 

_**Vedlegg 2:**_

Informasjon om innlevering. Hele mappen leveres som _**é**__**n**_ _**zip-fil**_.

 
 

_**Spørsmål 1)**_

Besvarelsen er én PDF/A-fil. Filen skal hete "sporsmal1.pdf"

 
 

_**Spørsmål**_ **2 –** **del 1****)**

Besvarelsen er én PDF/A-fil. Filen skal hete "sporsmal2_del1.pdf"

_**Spørsmål**_ **2 –** **del 2****)**

Besvarelsen er én PDF/A-fil. Filen skal hete "sporsmal2_del2.pdf"

_**Spørsmål**_ **2 –** **del 3****)**

Besvarelsen kan være en enkel XSD fil eller flere XSD filer. Du kan selv bestemme navnene på disse filene.

 
 

_**Spørsmål**_ _**3**_ _**–**_ _**del 1**__**)**_

Denne delen skal besvares i en mappe som heter "sporsmal_3". Innholdet i denne katalogen er beskrevet i Spørsmål 3 over og vises i figur 2.

_**Spørsmål**_ _**3**_ _**–**_ _**del 2**__**)**_

Besvarelsen er én PDF/A-fil. Filen skal hete "sporsmal3_2.pdf".

 
 

_**Spørsmål 4)**_

Besvarelsen er én PDF/A-fil. Filen skal hete "sporsmal4.pdf".

 
 

Alle filene kan være i samme mappe, dvs du trenger ikke å lage undermapper for hvert spørsmål. Dette vises i Figur 1. Hvis du ikke vet hvordan du lager en zip fil, er det viktig å snakke med faglærer i god tid før innleveringsfristen. Navnet på filen du skal laste opp vil være en kombinasjon av eksamensnummeret ditt (ikke studentnummer) og kurskode (ARK2200). Følgende er et eksempel filnavn:

_**ark2000_H21_kan_112.zip**_

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

Figur 1. Viser forventet innhold i mappen

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

 
 

Figur 2. Innholdet i Noark 5 uttrekket

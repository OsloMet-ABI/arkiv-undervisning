## ARK2500-H21 Arbeidskrav 2

Frist: 2021-10-30

**Bokmål**:

### 1. Noark 5 struktur

Lag en databasestruktur for følgende Noark 5 struktur: 

_arkiv, arkivdel, mappe, registrering,  dokumentbeskrivelse, dokumentobjekt_
 
Det er viktig at fremmednøkkel relasjoner er spesifisert riktig. Bruk Noark 5 standarden og metadatakatalogene for mer informasjon.

### 2. Insert kommandoer

Hver tabell skal ha minst en rad av informasjon. Metadatakatalogen kan hjelpe med å forstå innholdsverdiene feks hva dokumenttype betyr. Denne delen består av INSERT kommandoer.

For å bestå arbeidskravet må attributtnavn være slik de er spesifisert i metadatakatalogen og du skal kunne spore /se hele arkivstrukturen med bruk av systemID som primærnøkkel.

**Nynorsk**:

### 1. Noark 5 struktur

Lag ein database struktur for følgjande Noark 5 struktur:
 
_arkiv, arkivdel, mappe,_ _registrering,_ _dokumentbeskrivelse, dokumentobjekt_ 

Det er viktig at fremmednøkkel relasjonar er spesifisert riktig. Bruk Noark 5 standarden og metadatakatalogene for meir informasjon.

### 2. Insert kommandoar

Kvar tabell skal ha minst ei rad av informasjon. Metadatakatalogen kan hjelpa med å forstå innhaldsverdiane feks kva dokumenttype tyder. Denne delen består av INSERT kommandoar.

For å bestå arbeidskravet må attributtnamn vera slik dei er spesifisert i metadatakatalogen og du skal kunna spora /sjå heile arkivstrukturen med bruk av systemID som primærnøkkel.

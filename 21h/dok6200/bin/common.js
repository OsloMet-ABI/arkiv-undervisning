let app = angular.module('course', [])
    .directive('courseModalDir', function () {
      return {
        restrict: 'A',
        link: function (scope, element) {
          scope.dismissNewUserModal = function () {
          element.modal('hide');
          };
        }
      };
    });


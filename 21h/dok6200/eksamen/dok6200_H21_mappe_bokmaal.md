## DOK6200-H21 - Digital dokumentasjonsforvaltning
## Mappeeksamen

Eksamen består av tre spørsmål og alle spørsmålene må besvares. Alle delene teller likt.
 
## Spørsmål 1: 

En venn av deg har spurt deg om du kan se nærmere på datamodelleringsbehovene til sitt nye prosjekt, et selskap som vil tappe vann fra Reinheimen og selge det i miljøvennlig flasker. Han arvet et stort område i Reinheimen etter en gammel onkel og skal tappe vann fra innsjøer i området. Selskapet vil bli kalt "Aquaga" og etter en del diskusjon har daglig leder sendt deg følgende beskrivelse som hun tror kan brukes som grunnlag for en ER-modell.

_"Aquaga" ser en forretningsmulighet i å tappe vann fra forskjellie innsjøer ved Reinheimen : Tordsvatnet, Vermevatnet, Ulvålsdalvatnet og Storvatnet men flere vann kan bli tappet senere. Flaskene er laget av en unik og innovativt prosess der cellulose fra bambus, sukkerrot og tang blir kombinert til et miljøvennlig og nedbrytbar plast. Plasten har en begrenset holdbarhet før den naturlig vil starte en nedbrytingsprosses etter kontakt med vann. Foreløpig viser det seg at flaskene holder i 4 måneder. Derfor er det viktig å holde oversikt over utløpsdatoen for flaskene når de blir fyllt med vann. Vi har bestillt følgende flaskestørelser som skal brukes : 0.5L, 1.0L, 2.0L, 10.0L og 150.0L. Et nytt EU direktiv om sporbarhet for mineralvann gjør at vi må spore hver flaske vi fyller tilbake til kilde og tidspunkt flasken ble fyllt. 

Faktureringsprosessen vil bli håndtert av våre regnskapsførere og frakt blir tatt hånd om av underleverandør. Det som er viktig for oss er at vi har oversikt over hver flaske som blir fyllt der vannmengde, kilde og tidspunkt er registrert. Jeg ser frem til å se modellen din slik at vi kan komme i gang med modelleringsprossesen_

Dersom du finner noe uklart i beskrivelsen over, skriver du en kommentar om eventulle forutsetninger du har gjort. Når du lager det logiske skjemaet, skal primærnøkler understrekes, mens fremmednøkler merkes med en asterisk (*).


Oppgaver:

1. Lag ER-diagrammet for databasen og forklar resonnementet bak løsningen din. Gjør dine egne forutsetninger og gjør rede for disse dersom noe er uklart. Husk å understreke primærnøkler
2. Lag det logiske skjemaet til modellen. Husk å understreke primærnøkler og kursivere fremmednøkler.
3. Hvilken normalform er løsningen i? Forklar hvorfor.
4. Lag DDL-kommandoer på bakgrunn av det logiske skjemaet.
5. Lag DML-kommandoer som viser hvordan data settes inn i hvert tabell (maks 3 tupler per tabell).
6. Det skal være mulig å hente ut informasjon som viser antall flasker som er fyllt fra hver kilde for en bestemt dato. Formuler SQL-spørringen.


## Spørsmål 2: 

Du skal lage en Noark 5 databasestruktur som reflekterer arkivstrukturen i figur 1. Deretter skal du fylle relevant data i tabellene og utvikle noen SQL spørringer.

Besvarelsen vil bestå av følgende database kommandoer:

- CREATE DATABASE (DDL)
- CREATE TABLE (DDL)
- INSERT (DML)
- UPDATE (DML)
- SELECT (SQL)

For CREATE delen av oppgaven bruker du Figur 2 og Noark 5 metadatakatalogene. 
For INSERT delen av oppgaven skal arkivet inneholde følgende:

| arkivenhet            | antall  |
|-----------------------|---------|
| arkiv                 | 1       |
| arkivskaper           | 1       |
| arkivdel              | 1       |
| klassifikasjonsystem  | 1       |
| klasse                | 1       |
| saksmappe             | 1       |
| journalpost           | 2       |
| arkivnotat            | 1       |
| dokumentbeskrivelse   | 3       |
| dokumentobjekt        | 3       |

Tabell 1: Oversikt over antall arkivenheter i databasen

Merk! Saksmappen må ha minst et innkommende og utgående brev (2 journalposter) og et arkivnotat. Alle journalposter/arkivntater må ha både en dokumentbeskrivelse og et dokumentobjekt. Det elektroniske dokumentet antaes å være lagret i en mappe som heter dokumenter.

For UPDATE delen av oppgaven skal du lage SQL kommandoer som

- setter verdien av _saksstatus_ til «Avsluttet av skript» hvis _saksaar_ er før 2014 
- setter verdien av _formatDetaljer_ til «fmt/95» dersom _format_ har verdien «PDF»

For SELECT delen av oppgaven skal du lage SQL spørringer som kan gi en generell oversikt over:

- antall journalposter knyttet til de forskjellige saksmappene
- hvor mange duplikat dokumenter det finnes (se sjekksum)
- en _view_ som viser alle journalposter som er assosiert med en saksmappe. Som et minimum skal saksmappe_.tittel_ og journalpost_.tittel_ vises

**Tilleggsinformasjon**.

- Dette er et rent elektronisk arkiv så M301 oppbevaringssted skal ikke å være med. referanseArkivdel kan ansees å være 0-1, ikke 0-M for mappe og registrering
- virksomhetsspesifikkeMetadata er ikke relevant i dette spørsmålet og skal ikke være med
- Besvarelsen består av en tekstfil som inneholder alle kommandoer. Alle kommandoer i denne tekstfilen skal kunne kjøres feilfritt etter hverandre
- Attributtnavn skal følge Noark 5 metadatakatalogen. Dette gjelder også bruken av små og store bokstaver. Metadatakatalogen kan være nyttig til å forstå metadataelementene for å komme fram til fornuftige verdier
- Noark 5v5 skal brukes som grunnlag for oppgaven
- Du trenger ikke å lage de elektroniske dokumenter. Det holder med at referanseDokumentFil har en verdi som peker til et fiktiv dokument
- systemID skal være basert på UUID
- Arkivenheten arkivskaper skal ha en systemID attributt og arkivskaper 

![arkivstruktur](./figur1.png)
Figur 1. Oversikt over arkivstrukturen

## Spørsmål 3: 

### Del 1.

Du må lage et Noark 5 uttrekk med en struktur som er i samsvar med strukturen vist i vedlegg 1. Uttrekket må oppfylle alle kravene til et Noark 5 uttrekk slik de er definert i kapittel 6.4 av Noark 5-standarden (Noark 5 v 5).

Løsningen må inneholde følgende filer som du skal utvikle:

- arkivstruktur.xml 
- arkivutrekk.xml
- offentligjournal.xml
- loependejournal.xml
- endringslogg.xml
- info.xml

Gjeldende [XSD-filer](https://github.com/arkivverket/schemas/tree/master/N5/latest) lastes ned og inkluderes i uttrekket. arkivuttrekk.xml valideres med [addml.xsd](https://github.com/arkivverket/schemas/blob/master/ADDML/v8.2/addml.xsd).

En mappe med navnet _dokumenter_ må være med i uttrekket og alle dokumenter i denne katalogen må være i et arkivformat. Alle XML-filer må være velformede og gyldige. All data skal ha fornuftige verdier. Arkade5 kan brukes til å teste uttrekket ditt.

Filen arkivstruktur.xml må inneholde følgende:

- 1 arkiv
- 1 arkivskaper
- 1 arkivdel
- 1 klassifikasjonsystem
- 1 klasse
- 1 saksmappe
- 2 journalpost per saksmappe
- 1 arkivnotat per saksmappe
- 1 korrespondansePart per journalpost / arkivnotat
- 1 dokumentbeskrivelse per journalpost / arkivnotat
- 1 dokumentobjekt per dokumentbeskrivelse (skal være i arkivformat)

Filen endringslogg.xml trenger bare å inneholde to <endring> elementer med tilhørende data. Du skal vise endringer på <tittel> elementet for en journalpost og for en arkivnotat.

Filene lopendejournal.xml og offentligjournal.xml skal stemme overens med arkivstruktur.xml. Filen info.xml skal også være i samme mappe som uttrekket.

**Tilleggsinformasjon**. 

- Dette er et rent elektronisk arkiv så M301 oppbevaringssted skal ikke å være med. 
- referanseArkivdel kan ansees å være 0-1, ikke 0-M for mappe og registrering
- virksomhetsspesifikkeMetadata er ikke relevant i dette spørsmålet og skal ikke være med
- Besvarelsen består av en tekstfil som inneholder alle kommandoer. Alle kommandoer i denne tekstfilen skal kunne kjøres feilfritt etter hverandre
- Navn på elementer må følge Noark 5 metadatakatalogen. Dette gjelder også bruken av små og store bokstaver. Metadatakatalogen kan være nyttig til å forstå metadataelementene for å komme fram til fornuftige verdier
- Noark 5v5 skal brukes som grunnlag for oppgaven
- systemID skal være basert på UUID


### Del 2. _(2300 – 3400 tegn)_

Redegjør for hvordan et Noark 5 uttrekk utgjør et OAIS Submission Information Package.

## Informasjon om innlevering. 

Hele mappen leveres som _én_ zip-fil. 


### Spørsmål 1

 
### Spørsmål 2


### Spørsmål 3

#### Del 1
Denne delen skal besvares i en katalog/mappe som heter "sporsmal_3". Innholdet i denne katalogen er beskrevet i Spørsmål 3 over og vises i figur 2.

![uttrekk innhold](./figur2.png)
Figur 2. Innholdet i Noark 5 uttrekket

#### Del 2

Besvarelsen er én PDF/A-fil. Filen skal hete "sporsmal3_2.pdf".


Alle filene kan være i samme mappe, dvs du trenger ikke å lage undermapper for hvert spørsmål. Dette vises i Figur 3. Merk! Det vil legges ut et opptak som viser hvordan du lager en zip-fil. Ta kontakt med faglærer i god tid før innlevering dersom du opplever problemer med å lage en zip-fil. Navnet på filen du skal laste opp vil være en kombinasjon av eksamensnummeret ditt (ikke studentnummer) og kurskode (ARK2510). Følgende er et eksempel filnavn:

_ark2510_H21_kan_112.zip_



![katalog innhold](./figur3.png)
Figur 3. Forventet innhold i katalogen som skal zippes



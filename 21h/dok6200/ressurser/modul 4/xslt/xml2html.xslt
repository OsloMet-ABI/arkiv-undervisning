﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html"/>
  <xsl:template match="/">
    <html>
      <head>
        <title>
          <xsl:value-of select="/arkiv/tittel"/>
        </title>
      </head>
      <body>
        <h2>Liste av korrespondansepart</h2>
        <xsl:for-each select="//mappe">
          <hr/>
          <p>
            <input name="saksnr" type="text">
              <xsl:attribute name="value">
                <xsl:value-of select="mappeID"/>
              </xsl:attribute>
            </input>            
          </p>
          <p>
            <input name="tittel" type="text">
              <xsl:attribute name="value">
                <xsl:value-of select="tittel"/>
              </xsl:attribute>
            </input>            
          </p>
          <table border="1">
            <tr bgcolor="#e6b3cc">
              <th>korrespondansepartNavn</th>
              <th>korrespondanseparttype</th>
            </tr>
            <xsl:for-each select="registrering/korrespondansepart">
              <tr>
                <td>
                  <xsl:value-of select="korrespondansepartNavn"/>
                </td>
                <td>
                  <xsl:value-of select="korrespondanseparttype"/>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </xsl:for-each>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>


# Lag en liste over filformater i databasen

SELECT DISTINCT format FROM dokumentobjekt;


SELECT DISTINCT dokumentstatus FROM dokumentbeskivelse;


SELECT * FROM mappe 
	WHERE opprettetDato 
		BETWEEN '2021-01-01' AND '2021-12-31';

SELECT * FROM mappe 
	WHERE opprettetDato 
		BETWEEN '2021-01-01' AND '2021-12-31'
			ORDER BY opprettetDato ASC;

SELECT * FROM mappe 
	WHERE opprettetDato 
		BETWEEN '2021-01-01' AND '2021-12-31'
			ORDER BY opprettetAv DESC;


SELECT 
  TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME
FROM
  INFORMATION_SCHEMA.KEY_COLUMN_USAGE
WHERE
  REFERENCED_TABLE_SCHEMA = 's123456_noark5' AND
  REFERENCED_TABLE_NAME = 'mappe';


SELECT * FROM mappe, registrering
  WHERE mappe.systemID = 
    registrering.referanseMappe;


# Du kan begrense resultatsettet og spesifisere det maksimale antallet rader du vil ha tilbake
SELECT * FROM mappe LIMIT 3;

# Du kan også feks begrense resultatsett ved å ignorere de første to og ta med de neste 5
SELECT * FROM mappe LIMIT 2,5;

SELECT dokumenttype, COUNT( * ) AS 'Total'
	FROM dokumentbeskrivelse
		GROUP BY dokumenttype;

# Natual join eksempel

SELECT * FROM registrering NATURAL JOIN mappe; 

SELECT * FROM registrering, mappe WHERE mappe.systemID = registrering.referanseMappe;


SELECT * FROM registrering
 LEFT OUTER JOIN 	
  mappe ON
  registrering.referanseMappe = mappe.systemID;





# SQL / DDL brukt i forbindelse med DOK6200 for å lage en grunnleggende arkivstruktur


DROP DATABASE IF EXISTS s123456_noark5;
CREATE DATABASE s123456_noark5;

CREATE TABLE arkiv (
	systemID CHAR(36) NOT NULL,
	tittel VARCHAR(100) NOT NULL,
	beskrivelse VARCHAR(100),
	arkivstatus CHAR(20),
	dokumentmedium VARCHAR(50),
	opprettetDato DATETIME NOT NULL,
	opprettetAv VARCHAR(100) NOT NULL,
	avsluttetDato DATETIME,
	avsluttetAv VARCHAR(100),
	PRIMARY KEY(systemID)
 );


CREATE TABLE arkivdel (
	systemID CHAR(36),
	tittel VARCHAR(100) NOT NULL,
	beskrivelse VARCHAR(100),
	arkivdelstatus CHAR(20) NOT NULL,
	dokumentmedium VARCHAR(50),
	opprettetDato DATETIME NOT NULL,
	opprettetAv VARCHAR(100) NOT NULL,
	avsluttetDato DATETIME,
	avsluttetAv VARCHAR(100),
	arkivperiodeStartDato DATE,
	arkivperiodeSluttDato DATE,
	referanseForloeper CHAR(36),
	referanseArvtaker CHAR(36),
	referanseArkiv CHAR(36) NOT NULL,
	referanseKlassifikasjonssystem CHAR(36),
	PRIMARY KEY (systemID)
 );


CREATE TABLE klassifikasjonssystem(
	systemID CHAR(36),
	klassifikasjonstype VARCHAR(100),
	tittel VARCHAR(100),
	beskrivelse VARCHAR(100),
	opprettetDato DATETIME,
	opprettetAv VARCHAR(100),
	avsluttetDato DATETIME,
	avsluttetAv VARCHAR(100),
	PRIMARY KEY (systemID)
);


CREATE TABLE klasse (
	systemID CHAR(36) NOT NULL,
	klasseId CHAR(36) NOT NULL,
	tittel VARCHAR(100),
	beskrivelse VARCHAR(100),
	opprettetDato DATETIME NOT NULL,
	opprettetAv VARCHAR(100) NOT NULL,
	avsluttetDato DATETIME,
	avsluttetAv VARCHAR(100),
	referanseKlassifikasjonssystem CHAR(36),
	referanseKlasse CHAR(36),
	PRIMARY KEY (systemID)
 );

CREATE TABLE mappe (
	systemID CHAR(36) NOT NULL,
	mappeId CHAR(36) NOT NULL,
	tittel VARCHAR(100),
	offentligTittel VARCHAR(100),
	beskrivelse VARCHAR(100),
	dokumentmedium VARCHAR(50),
	opprettetDato DATETIME NOT NULL,
	opprettetAv VARCHAR(100) NOT NULL,
	avsluttetDato DATETIME,
	avsluttetAv VARCHAR(100),
	referanseArkivdel CHAR(36),
	referanseKlasse CHAR(36),	
	PRIMARY KEY (systemID)
 );

CREATE TABLE registrering (
	systemID CHAR(36),
	opprettetDato DATETIME NOT NULL,
	opprettetAv VARCHAR(100) NOT NULL,
	arkivertDato DATETIME,
	arkivertAv VARCHAR(100),
	referanseArkivdel CHAR(36),
	registreringsID CHAR(100),
	tittel VARCHAR(100),
	offentligTittel VARCHAR(100),
	beskrivelse VARCHAR(100),
	noekkelord VARCHAR(100),
	forfatter VARCHAR(100),
	dokumentmedium VARCHAR(50),
	oppbevaringssted VARCHAR(50),
	referanseMappe CHAR(36),
	referanseKlasse CHAR(36),
	PRIMARY KEY (systemID)
 );


CREATE TABLE dokumentbeskrivelse (
	systemID CHAR(36),
	dokumenttype VARCHAR(100)  NOT NULL,
	dokumentstatus VARCHAR(100)  NOT NULL,
	tittel VARCHAR(100),
	beskrivelse VARCHAR(100),
	forfatter VARCHAR(255),
	opprettetDato DATETIME,
	opprettetAv VARCHAR(100),
	dokumentmedium VARCHAR(50),
	referanseArkivdel CHAR(36),
	tilknyttetRegistreringSom VARCHAR(100)  NOT NULL,
	tilknyttetDato DATETIME,
	tilknyttetAv VARCHAR(100),
	dokumentnummer INT,
	referanseRegistrering CHAR(36),
	PRIMARY KEY (systemID)
 );

CREATE TABLE dokumentobjekt (
	systemID CHAR(36),
	versjonsnummer INT NOT NULL,
	variantformat VARCHAR(255) NOT NULL,
	format VARCHAR(255) NOT NULL,
	formatDetaljer VARCHAR(255) ,
	opprettetDato DATETIME NOT NULL,
	opprettetAv VARCHAR(255) NOT NULL,
	referanseDokumentfil VARCHAR(255) NOT NULL,
	sjekksum VARCHAR(255) NOT NULL,
	sjekksumAlgoritme VARCHAR(255) NOT NULL,
	filstoerrelse INTEGER NOT NULL,
	referanseDokumentbeskrivelse CHAR(36),
	PRIMARY KEY (systemID)	
 );


ALTER TABLE  arkivdel 
	ADD FOREIGN KEY (referanseArkiv) 
		REFERENCES arkiv (systemID);

# Koble arkivdelen til klassifikasjonssystemet
ALTER TABLE  arkivdel 
	ADD FOREIGN KEY (referanseKlassifikasjonssystem) 
		REFERENCES klassifikasjonssystem(systemID);

# Koble klassen til klassifikasjonssystemet
ALTER TABLE  klasse 
	ADD FOREIGN KEY (referanseKlassifikasjonssystem) 
		REFERENCES klassifikasjonssystem (systemID);

# Legg til en 'intern' fremmednøkkel
ALTER TABLE  klasse 
	ADD FOREIGN KEY (referanseKlasse) 
		REFERENCES klasse (systemID);

ALTER TABLE  mappe
	ADD FOREIGN KEY (referanseKlasse) 
		REFERENCES klasse (systemID),
	ADD FOREIGN KEY (referanseArkivdel) 
		REFERENCES arkivdel (systemID);


ALTER TABLE  registrering 
	ADD FOREIGN KEY (referanseArkivdel) 
		REFERENCES arkivdel (systemID),
	ADD FOREIGN KEY (referanseMappe) 
		REFERENCES mappe (systemID),
	ADD FOREIGN KEY (referanseKlasse) 
		REFERENCES klasse (systemID);

ALTER TABLE dokumentbeskrivelse 
	ADD FOREIGN KEY (referanseRegistrering) 
		REFERENCES registrering (systemID);

ALTER TABLE  dokumentobjekt 
	ADD FOREIGN KEY (referanseDokumentbeskrivelse) 
  		REFERENCES dokumentbeskrivelse (systemID);

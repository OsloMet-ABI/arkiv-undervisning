## DOK6200-H21 Arbeidskrav 2

Frist: 2021-10-24

**Bokmål:**

Du må lage filen _arkivstruktur.xml_ der innholdet gjenspeiler strukturen vist i figur 1. Filen arkivstruktur.xml skal inneholde følgende Noark 5 objekter:

- 1 arkiv
- 1 arkivdel
- 1 saksmappe 
- 1 journalpost (et innkommende dokument)
- 1 dokumentbeskrivelse
- 1 dokumentobjekt

Merk, arbeidskravet består kun av filen arkivstruktur.xml.

**Nynorsk**:

Du må laga fila arkivstruktur.xml der innhaldet gjenspeglar strukturen figur i vedlegg 1 . Fila arkivstruktur.xml skal innehalda følgjande Noark 5 objekt:

- 1 arkiv
- 1 arkivdel
- 1 saksmappe
- 1 journalpost (et innkommende dokument)
- 1 dokumentbeskrivelse
- 1 dokumentobjekt

Merk, arbeidskravet består berre av fila arkivstruktur.xml.


![arkivstruktur](./arkivstruktur.png)
***Figur 1** : arkivstruktur som skal lages* 


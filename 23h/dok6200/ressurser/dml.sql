INSERT INTO bil (
	registreringsNr, understellsNr, farge, produsent, modell
) 
VALUES (
	"ZQ10000", "111592315", "svart", "Audi", "A3"
);


INSERT INTO bil (
	registreringsNr, understellsNr, farge, produsent, modell
) 
VALUES (
	"ZQ10001", "111592316", "svart", "Audi", "A3"
), (
	"ZQ10002", "136695919", "rød", "VW", "Gold"
);

UPDATE bil SET 
	registreringsNr = "ZQ10099" 
WHERE 
	registreringsNr = "ZQ10000";


UPDATE bil SET 
	farge = "rosa" 
WHERE 
	registreringsNr = "ZQ10099" ;


UPDATE bil SET 
	farge = "rød";

DELETE FROM bil 
	WHERE 
	registreringsNr = "ZQ10099";

DELETE FROM bil;

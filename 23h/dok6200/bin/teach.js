app.controller('CourseController',
    ['$scope', function ($scope) {

        var moduleData = [
                {
                    mid: 1,
                    module_name: "Emnet og diverse",
                    module_description: "Her finnes informasjon om emnet, webinarene og andre opptak som legges ut i løpet av semesteret",
                    module_finish: "2023-11-25",
                    sub_modules: [
                        {
                            sub_module_id: 1,
                            sub_module_name: "Om emnet 2023-08-21",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Oppstartswebinar",
                                    lecture_description: "Overordnet introduksjon og beskrivelse av emnet",
                                    lecture_comment: "",
                                    duration: "108m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=0ad8e259-4ef2-46f2-9ce2-b066012f07f5"
                                }
                            ]
                        },
                        {
                            sub_module_id: 2,
                            sub_module_name: "Webinarer",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Webinar 1 2023-09-04",
                                    lecture_description: "",
                                    lecture_comment: "",
                                    duration: "97m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=8f67de71-8cbb-4bc8-b348-b07400790d84"
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Webinar 2 2023-09-18",
                                    lecture_description: "Fra modul 1 til modul 2",
                                    lecture_comment: "Spørsmål om databaser og normalisering. Oppsummering av modul 1 og intro til modul 2.",
                                    duration: "114m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=f584c6ba-302e-44fc-bd73-b0810101cb6c	"
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Webinar 3 2023-10-09",
                                    lecture_description: "Litt om arbeidskrav 1, oppsummering av Noark og intro til neste modul",
                                    lecture_comment: "",
                                    duration: "97m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=94e304fe-327a-4eda-8624-b09601062b87"
                                },
                                {
                                    lid: 4,
                                    lecture_name: "Webinar 4 2023-10-23",
                                    lecture_description: "",
                                    lecture_comment: "Litt av hvert. Overordnet om arbeidskrav 2, IMDB databasen, nikita peek. Perspektiver og neste modul",
                                    duration: "138m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=c6c28565-d12c-4d99-be95-b0a400df93c3"
                                },
                                {
                                    lid: 5,
                                    lecture_name: "Webinar 5 2023-11-06",
                                    lecture_description: "",
                                    lecture_comment: "Midtveisevaluering, litt om eksamen. Oppsummere modul 4 og introdusere modul 5.",
                                    duration: "98m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=08df5f0b-9de5-40ce-b16d-b0b20136d830"
                                },
                                {
                                    lid: 6,
                                    lecture_name: "Webinar 6 2023-11-21",
                                    lecture_description: "",
                                    lecture_comment: "Litt om arbeidskrav 3, spørsmål fra Discussion og oppsumering",
                                    duration: "101m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Viewer.aspx?id=24307f58-d1a8-4101-b279-b0c1010040e3"
                                }
                            ]
                        },
                        {
                            sub_module_id: 3,
                            sub_module_name: "Spørsmål og svar",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "",
                                    lecture_description: "",
                                    lecture_comment: "",
                                    duration: "",
                                    href: ""
                                }
                            ]
                        }
                    ]
                },
                {
                    mid: 2,
                    module_name: "Databaser og datamodellering",
                    module_description: "Historikken til databaser, ER-modellering, relasjonsmodellen, SQL",
                    module_finish: "2023-09-09",
                    sub_modules: [
                        {
                            sub_module_id: 1,
                            sub_module_name: "Introduksjon",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Historien til databaser",
                                    lecture_description: "En introduksjon som forteller litt om hvor det vi opplever som moderne databaser kommer fra",
                                    lecture_comment: "",
                                    duration: "19m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=6349bb1b55c74e879bbeaf0100733a48",
                                    slide_href: "foiler/modul1/Kort om historien til databaser.odp"
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Hvorfor bruker vi databaser?",
                                    lecture_description: "Vi bruker databaser fordi de har en del egenskaper som er viktig for å sikre integriteten til data som lagres.",
                                    lecture_comment: "",
                                    duration: "34m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=5b5da666e4e64ae3b139aeff003e9f01",
                                    slide_href: "foiler/modul1/Hvorfor bruker vi databaser.odp"
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Hva er en database?",
                                    lecture_description: "En database er en strukturert / organisert samling av data lagret (elektronisk) og gjort tilgjengelig via et datasystem",
                                    lecture_comment: "",
                                    duration: "6m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=561afece6fde4c7bb06eaf0300a670b7",
                                    slide_href: ""
                                },
                                {
                                    lid: 4,
                                    lecture_name: "Relasjonsmodellen",
                                    lecture_description: "Relasjonsmodellen er en modell som mange moderne DBHS er implemtert ettet. Her se vi litt på modellen og noe av terminioligien som er viktig å ha med seg videre i emnet.",
                                    lecture_comment: "",
                                    duration: "20m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=a94e5e5d31ce4ecf8aefaef4013e26ae",
                                    slide_href: ""
                                },
                                {
                                    lid: 5,
                                    lecture_name: "Tilgang til MySQL",
                                    lecture_description: "Hvordan du får tilgang til MySQL serveren som vi jobber med",
                                    lecture_comment: 'MySQL via phpmyadmin (https://bibin.oslomet.no/phpmyadmin/), Opprett konto (https://bibin.oslomet.no/sql/). Merk du må være student for å få tilgang',
                                    duration: "7m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=4de7f68617f4423db9c3aef7006bdfab",
                                    slide_href: ""
                                }
                            ]
                        },
                        {
                            sub_module_id: 2,
                            sub_module_name: "Structured Query Language (SQL)",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Introduksjon",
                                    lecture_description: "Hva er SQL og hvordan brukes det. Vi tar også en gjennomgang av phpmyadmin og ser litt på redigeringsprogrammer som vi bruker for å utvikle SQL spørringer",
                                    lecture_comment: "Windows: Bruk notepad++. Mac: Netbeans, atom. Linux: gedit, netbeans, atom",
                                    duration: "23m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=fed3527e519c4e429ce2aef400c8ae05",
                                    slide_href: ""
                                },
                                {
                                    lid: 2,
                                    lecture_name: "SQL synktaks og bruk 1",
                                    lecture_description: "Grunnleggende SQL og litt om filtrering med WHERE og bruken av jokertegn",
                                    lecture_comment: "",
                                    duration: "28m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=86d64fcc2f67461383fdaeff0157690a",
                                    slide_href: ""
                                },
                                {
                                    lid: 3,
                                    lecture_name: "SQL synktaks og bruk 2",
                                    lecture_description: "Mer om filtrering med LIMIT, DISTINCT, ORDER BY og GROUP BY",
                                    lecture_comment: "",
                                    duration: "28m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=1da09c719b8648cda6e5aefe008a1960",
                                    slide_href: ""
                                },
                                {
                                    lid: 4,
                                    lecture_name: "Joins",
                                    lecture_description: "Databasetabeller inneholder ofte informasjon som er koblet sammen med fremmednøkler. Her ser vi hvordan vi kan jobbe med sammenkoblet data",
                                    lecture_comment: "",
                                    duration: "25m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=b25ad705f2fa4aba9ad9aefe00b510f2",
                                    slide_href: ""
                                },
                                {
                                    lid: 5,
                                    lecture_name: "SQL visninger",
                                    lecture_description: "En SQL visning er en virtuell tabell som baseres på resultat av en SQL spørring",
                                    lecture_comment: "",
                                    duration: "4m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=f2b44494a5d247498261af0000471e4a",
                                    slide_href: ""
                                }
                            ]
                        },
                        {
                            sub_module_id: 3,
                            sub_module_name: "ER-modellering og normalisering",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "ER-modellering",
                                    lecture_description: "Modellering er et viktig steg i prossesen med å lage en database",
                                    lecture_comment: "",
                                    duration: "50m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=be777759f57243dca19eaeff00d8ad4f",
                                    slide_href: ""
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Normalisering",
                                    lecture_description: "Hva er normalisering og hvorfor er det viktig?",
                                    lecture_comment: "",
                                    duration: "58m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=15de164b13824a1abf4aaeff00a04bcc",
                                    slide_href: ""
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Logisk skjema",
                                    lecture_description: "Utvikling av logisk skjema er siste steg for å bygge databasestrukturen",
                                    lecture_comment: "",
                                    duration: "12m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=2b3752b9b9eb449b8e9faf0100a95b35",
                                    slide_href: ""
                                }
                            ]
                        },
                        {
                            sub_module_id: 4,
                            sub_module_name: "DDL og DML",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Datatyper brukt i databaser",
                                    lecture_description: "Attributter må ha en datatype definert når tabellen opprettes",
                                    lecture_comment: "",
                                    duration: "27m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=f10e185f6f5849339702aefc00aa6add",
                                    slide_href: ""
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Data Definition Language (DDL)",
                                    lecture_description: "DDL er verktøyet vi bruker for å opprette, slette og endre database struktur",
                                    lecture_comment: "",
                                    duration: "49m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=5ecc791e9906481e8e3aaf01017dc60d",
                                    slide_href: ""
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Data Manipulation Language (DML)",
                                    lecture_description: "DML brukes til å sette inn, endre og slette innhold i en tabell",
                                    lecture_comment: "",
                                    duration: "43m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=626beb9fcff94641a295aeff013416b5",
                                    slide_href: ""
                                }
                            ]
                        },
                        {
                            sub_module_id: 4,
                            sub_module_name: "Oppsummering",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Modul 1 oppsummert",
                                    lecture_description: "En oppsummring av det somer gjennomgått i modul 1",
                                    lecture_comment: "",
                                    duration: "6m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=3ab3a3af4a2a48e08933aefa0053d2cf"
                                }
                            ]
                        }
                    ]
                },
                {
                    mid: 3,
                    module_name: "Dokumentasjonsforvaltning",
                    module_description: "Implementasjon av Noark i database og diskuterer diverse perspektiver rundt dette.",
                    module_finish: "2023-09-23",
                    sub_modules: [
                        {
                            sub_module_id: 1,
                            sub_module_name: "Introduksjon",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Introduksjon",
                                    lecture_description: "Modulen bygger på det som er gjennomgått i modul 1 og lar oss ta i bruke det vi lærte i praksis",
                                    lecture_comment: "",
                                    duration: "3m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=f83a0ec56bf3441fa3ddaefa00c54cd5",
                                    slide_href: ""
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Intro til Noark og dokumentasjonsforvaltning",
                                    lecture_description: "Noark er en standard som kan brukes som en 'best-practice' for  dokumentasjonsforvaltning. Her begynner vi med en fugleperspektiv på hva Noark 5 er og hvordan det kan være en veileder for  dokumentasjonsforvaltning",
                                    lecture_comment: "",
                                    duration: "50m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=bbf8b9342a2b4ff8a948af010147976b",
                                    slide_href: ""
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Intro til arkivstruktur",
                                    lecture_description: "Datamodellen til Noark (arkivstruktur) er en av de viktigste delene av Noark",
                                    lecture_comment: "",
                                    duration: "50m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=24b9745e393746efb378aeff0097ee6b",
                                    slide_href: ""
                                }
                            ]
                        },
                        {
                            sub_module_id: 2,
                            sub_module_name: "Noark 5 DDL",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Arkiv og arkivdel",
                                    lecture_description: "Metadata til arkiv/arkivdel og hvordan de kobles med en fremmednøkkel. Litt diskusjon rundt hvorfor det kanskje ikke er relevant for et fagsystem. Litt om hvordan moderne samhandlingsplatformer er Noark vennlig.",
                                    lecture_comment: "",
                                    duration: "71m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=589f05fc77d2493a9268aefb01348997",
                                    slide_href: ""
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Klassifikasjonssytem og klasse",
                                    lecture_description: "Metadata til klassifikasjonssytem og klasse og hvordan de kobles med en fremmednøkkel. Litt mer om interne fremmednøkler",
                                    lecture_comment: "",
                                    duration: "32m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=2b9efd6b4361453cba7eaf01017c181b",
                                    slide_href: ""
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Mappe og registrering",
                                    lecture_description: "Metadata til mappe og registrering og hvordan de utgjør en transaksjonskontekst. Mappe og registrering kan utvides til spesialisering (arv)",
                                    lecture_comment: "",
                                    duration: "44m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=0cd363522a584867bce1aefe0137cd02",
                                    slide_href: ""
                                },
                                {
                                    lid: 4,
                                    lecture_name: "Dokumentbeskrivelse og dokumentobjekt",
                                    lecture_description: "Metadata til dokumentbeskrivelse og dokumentobjekt og hva en dokumentkontekst er og hvorfor det er viktig for elektronisk materiale. En forklaring på hva en sjekksumm er. En liten oppsummering av arkivstruktur",
                                    lecture_comment: "",
                                    duration: "50m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=3e1f1329e53c4525bc0aaefc00237864",
                                    slide_href: ""
                                }
                            ]
                        },
                        {
                            sub_module_id: 3,
                            sub_module_name: "Noark 5 DDL",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Noark 5 og DDL",
                                    lecture_description: "Hvordan sette Noark data inn i en tabell. Litt om forksjellen mellom rollene bruker, system og database her på å angi verdier",
                                    lecture_comment: "",
                                    duration: "38m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=d3d7fba986a749dc9932aefa00c87a01",
                                    slide_href: ""
                                }
                            ]
                        },
                        {
                            sub_module_id: 4,
                            sub_module_name: "Noark 5 SQL",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Noark 5 og SQL",
                                    lecture_description: "Overordnet gjennomgang av SQL søk med Noark og hva som er interessant å kunne hente ut. Spesielt fokus på joins da disse har en viktig rolle når uttrekk lages. Inkluderer også perspektiver rundt det å jobbe i en database ut ifra en Noark kontekst.",
                                    lecture_comment: "",
                                    duration: "44m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=78b42f35afb74664b5b5af0000c33532",
                                    slide_href: ""
                                }
                            ]
                        }
                    ]
                },
                {
                    mid: 4,
                    module_name: "Perspektiver",
                    module_description: "Her går vi litt bredere og ser mer på litt teori rundt praksis",
                    module_finish: "2023-10-15",
                    sub_modules: [
                        {
                            sub_module_id: 1,
                            sub_module_name: "Introduksjon",
                            lectures: [{
                                lid: 1,
                                lecture_name: "Introduksjon",
                                lecture_description: "4 artikler legges vekt på for å danne en teroretisk kontekst for emnet",
                                lecture_comment: "",
                                duration: "7m",
                                href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=c8f4733515f74d25afe4aeff00a1f0f9"
                            }
                            ]
                        }, {
                            sub_module_id: 2,
                            sub_module_name: "Hurley. What if anything ...",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Hva er er dokumentasjonsforvaltning?",
                                    lecture_description: "Hurley stiller spørsmål til hva dokumentasjonsforvaltning er og hvordan det påvirkes av endringer.",
                                    lecture_comment: "",
                                    duration: "38m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=36dab0fbc77049d48e20aefe000dd135"
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Utviklingen som skapte en usikker tid",
                                    lecture_description: "Hva skjedde når 'ansatte' ble 'brukere'?. Litt mer om dokumentasjon.",
                                    lecture_comment: "",
                                    duration: "50m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=58fccefd0bc448eab5d0aeff00cbc017"
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Fra digitalisering til rekonstruksjon",
                                    lecture_description: "Digitalisering eller automatisering, er ikke målet. Evnen til rekonstruksjon er det som er viktig.",
                                    lecture_comment: "",
                                    duration: "23m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=da68e95d09fd4150bcd5af010046389b"
                                },
                                {
                                    lid: 4,
                                    lecture_name: "Forretnings- og dokumentasjonsprosesser",
                                    lecture_description: "Forholdet mellom forretnings- og dokumentasjonsprosesser. Hva tilbyr Noark i et slikt perspektiv.",
                                    lecture_comment: "",
                                    duration: "38m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=12bb79b6be3146b7943caf02010661d8"
                                },
                                {
                                    lid: 5,
                                    lecture_name: "Hurley i en norsk kontekst",
                                    lecture_description: "Litt om det Hurley ikke kun si så mye om i 2004. Personvern, nettsky osv. Hvilken perspektiv er egentlig det som er vikitg? Bevis, bevaring, prosses eller søk?",
                                    lecture_comment: "",
                                    duration: "40m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=8dc434c8bd1c4c40b399aefc0127e39b"
                                }


                            ]
                        },
                        {
                            sub_module_id: 3,
                            sub_module_name: "Duranti. Concepts, principles ...",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Utfordringer med dokumentasjonsforvaltning",
                                    lecture_description: "Hvorfor dokumentasjonsforvaltning vanskelig?",
                                    lecture_comment: "",
                                    duration: "54m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=5bf67695b5c94d3e9644aefc01427972"
                                }, {
                                    lid: 2,
                                    lecture_name: "Sikre pålitlighet og autentisitet",
                                    lecture_description: "Mer fokus på autentisitet. En bredere definasjon av egenskapene til et arkivdokument som er kanskje bredere en det vi er vant til. Signraturer og andre mekanismer som sikrer pålitlighet.",
                                    lecture_comment: "",
                                    duration: "64m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=0d1aec6ddc2f4c4dafc4aeff0026aff9"
                                }
                            ]
                        },
                        {
                            sub_module_id: 4,
                            sub_module_name: "McDonald. RM/DM closing the gap ",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Dokumentasjonsforvaltning og dataledelse",
                                    lecture_description: "To profesjoner som forvalter data kniver om retten til å bestemme. Er det bare en som kan vinne?",
                                    lecture_comment: "",
                                    duration: "46m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=0c89a06ba38d4fcf91afaeff0169af24"
                                }
                            ]
                        },
                        {
                            sub_module_id: 5,
                            sub_module_name: "Sødring IoT",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "IoT utfordringer",
                                    lecture_description: "Hva er IoT og hvorfor kan det være problematisk for dokumentasjonsforvaltning?",
                                    lecture_comment: "",
                                    duration: "32m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=ee1288689f8845bb9c5daef6014d3b7f"
                                }, {
                                    lid: 2,
                                    lecture_name: "GARP prinsippene og case",
                                    lecture_description: "Her presenterer vi caset som artikkelen baserer seg på. Deretter diskuterer vi GARP-prinsippene og tar de i bruk for caset.",
                                    lecture_comment: "",
                                    duration: "40m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=0ceeabd975294575ab76aeff014a7e60"
                                }, {
                                    lid: 3,
                                    lecture_name: "GARP i Norge?",
                                    lecture_description: "Kan Noark brukes i begge casene? Hvilken prinsipper bruker vi i Norge? Danning og bevaring er to sider av samme sak. Ser vi en helhet eller bar stykkvis",
                                    lecture_comment: "",
                                    duration: "49m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=f68f5097677c4a088846aeff015e25f8"
                                }
                            ]
                        }
                    ]
                },
                {
                    mid: 5,
                    module_name: "Bevaring av dokumentasjon",
                    module_description: "Hvordan bevarer vi dokumentasjon? Hvilken formater bruker vi og hvordan kan vi migrere data for bevaring",
                    module_finish: "2023-11-05",
                    sub_modules: [
                        {
                            sub_module_id: 1,
                            sub_module_name: "Introduksjon",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Introduksjon",
                                    lecture_description: "I modulen vil vi gå gjennom XML og XSD og samtidig sette det i et arkivfaglig kontekst.",
                                    lecture_comment: "",
                                    duration: "2m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=513d59560ef14963a350af0201511c68"
                                }
                            ]
                        },
                        {
                            sub_module_id: 2,
                            sub_module_name: "XML",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Introduksjon til XML",
                                    lecture_description: "Hva er et markeringspråk, hvor kommder det fra og hvorfor er det viktig i et arkivsammenheng? Litt forskjelige tilnærminger til å overføre data med vekt på XML",
                                    lecture_comment: "",
                                    duration: "51m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=9c64fedb619f449b8c65af0100319023"
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Oppbygging av et XML dokument",
                                    lecture_description: "Et XML dokument består av prolog og rotelement/dokumentelement. Litt om verktøy som kan brukes og vår første XML-dokument",
                                    lecture_comment: "",
                                    duration: "22m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=1b4daaa7d5764348afbfaf02014c1053"
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Utvikling av XML dokumenter",
                                    lecture_description: "Fortsetter med flere eksempler og gradvis bygger ut hva vi beskrive i XML dokumentet. Litt om hvordan du må samhandle med verktøyet.",
                                    lecture_comment: "",
                                    duration: "33m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=d9b2756056634e73a843aefa00986312"
                                },
                                {
                                    lid: 4,
                                    lecture_name: "XML videre",
                                    lecture_description: "Utvikler et XML-dokument som kan beskrive en bok. Ser litt på hvordan struktur blir viktig. Tar opp forholdet XML/database der det viktig å definere et element som avgrenser en rad av informasjon i en database tabell",
                                    lecture_comment: "Struktur og validering er noe som kommer i modulen om XSD",
                                    duration: "22m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=6a51b9fd9d8e4fd0909baf010126171a"
                                },
                                {
                                    lid: 5,
                                    lecture_name: "XML-'stacken'",
                                    lecture_description: "Du må se på XML i forhold til helheten det tilbyr. Ser litt på xpath og xslt.",
                                    lecture_comment: "Dette er ikke noe det forventes at dere skal lære, eller kunne gjøre selv. Det vises for å gi et inntrykk i hva XML-stacken kan gjøre.",
                                    duration: "26m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=82ba32c3a2de4a76a0b6af0000c66ff7"
                                }
                            ]
                        },

                        {
                            sub_module_id: 3,
                            sub_module_name: "XSD",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Introduksjon til XSD",
                                    lecture_description: "Hvorfor er det viktig å definere strutur til XML dokumenter? Innom dette med korrekt strukturet og gyldighet.",
                                    lecture_comment: "",
                                    duration: "31m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=af415f0dced448a19cceaefd01002fbf"
                                },
                                {
                                    lid: 2,
                                    lecture_name: "Korrekt strukturert",
                                    lecture_description: "Hvilken regler gjelder for oppbygging og syntaks av et XML-dokument",
                                    lecture_comment: "",
                                    duration: "41m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=da0981e4508340b29a0faf0000472598"
                                },
                                {
                                    lid: 3,
                                    lecture_name: "Litt mer om XSD",
                                    lecture_description: "Hva XSD kan gjøre og litt om interoperabilitet. Datatyper brukt i XSD",
                                    lecture_comment: "",
                                    duration: "23m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=306d55e5ec524d799e4faefc001ae011"
                                },
                                {
                                    lid: 4,
                                    lecture_name: "Utvikle XSD og test  av gyldiget",
                                    lecture_description: "Hvordan du bygger opp en XSD-fil. Hvordan kobles XML/XSD sammen for validering",
                                    lecture_comment: "",
                                    duration: "34m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=11f66c70eb8c4fbaa866af00001814d5"
                                },
                                {
                                    lid: 5,
                                    lecture_name: "Struktur og forekomster",
                                    lecture_description: "Struktur defineres med bruken av complexType, mens min/maxOccurs brukes til å si hvor mange ganger et element kan forekomme",
                                    lecture_comment: "",
                                    duration: "51m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=5b7ae0e807a94f819ec9aefc008bf0df"
                                },
                                {
                                    lid: 6,
                                    lecture_name: "XSD restriksjoner",
                                    lecture_description: "XSD restriksjoner kan si noe om gyldigheten til innholdet i XML-dokumentet. Tidligere har vi sett på gyldighet i forhold til struktur og forekomst av elementer. Her blir fokus på gydlighet til innhold.",
                                    lecture_comment: "",
                                    duration: "42m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=fb2258a510d94a00adabaf01004eab61"
                                },
                                {
                                    lid: 7,
                                    lecture_name: "Andre måter å bygge XSD",
                                    lecture_description: "Vi har lært en russian doll tilnærming for å lage XSD. Det finnes andre måter og Noark bruke en av disse andre tilnærmingene.",
                                    lecture_comment: "Ikke noe alle trenger å vite, men gir en forståelse av hvorfor Arkivverket ikke bruker Russian doll tilnærmingen.",
                                    duration: "14m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=98ccdcd08ead49918e17aeff01104c47"
                                },
                                {
                                    lid: 8,
                                    lecture_name: "Hvordan generere XSD fra XML",
                                    lecture_description: "Dersom du har et XML dokument kan du genrere en XSD for å få oversikt over strukturen.",
                                    lecture_comment: "",
                                    duration: "10m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=0ba38b54dc95401c89a5af0001829b49"
                                },
                                {
                                    lid: 9,
                                    lecture_name: "Oppsummering",
                                    lecture_description: "Dersom du har et XML dokument kan du genrere en XSD for å få oversikt over strukturen.",
                                    lecture_comment: "",
                                    duration: "10m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=5b494c6991294651b63aaf0101012d01"
                                }
                            ]
                        }
                    ]
                },
                {
                    mid: 6,
                    module_name: "Bevaring av databaser",
                    module_description: "Databaser kan lagres i formater som ADDML eller SIARD. Her ser vi på SIARD og hvordan det kan brukes til bevaring.",
                    module_finish: "2023-11-25",
                    sub_modules: [
                        {
                            sub_module_id: 1,
                            sub_module_name: "Introduksjon",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Bevaringstrategier",
                                    lecture_description: "En introduksjon til modulen der vi tar utgangspunkt i forskjellig bevaringstrategier og lander på den kontroversielle strategien migrering.",
                                    lecture_comment: "",
                                    duration: "30m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=9e9783d923114cf49db5af0001451092"
                                }, {
                                    lid: 2,
                                    lecture_name: "Opprettholde forståelighet",
                                    lecture_description: "Det kan være vanskelig å opprettholde forståeligheten til dokumentasjon når det migreres.",
                                    lecture_comment: "",
                                    duration: "35m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=07841ab632de445f9056af02016829e4"
                                }, {
                                    lid: 3,
                                    lecture_name: "Semantiske eller tabelluttrekk",
                                    lecture_description: "To tilnaærminger til uttrekk fra database finnes. Semantiske (feks Noark) eller tabelluttrekk (feks SIARD). Vi ser på forskjellen og litt hvordan SIARD kan løftes til en forstå nivå",
                                    lecture_comment: "",
                                    duration: "56m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=289f727692604958a800aefc012cd0d6"
                                }
                            ]
                        },
                        {
                            sub_module_id: 2,
                            sub_module_name: "Noark 5 uttrekk",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Noark som OAIS pakke",
                                    lecture_description: "OAIS standarden definerer egenskapene til en arkivpakke. Her blir de gjennomgått. ",
                                    lecture_comment: "Merk. Opptaket skjedde på feil skjerm!",
                                    duration: "46m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=1ce66c48853d4a9d9081aef70009300b"
                                }, {
                                    lid: 2,
                                    lecture_name: "Overordnet beskrivelse",
                                    lecture_description: "En beskrivelse av de overodnet kravene til et Noark 5 uttrekk",
                                    lecture_comment: "Merk. Opptaket skjedde på feil skjerm!",
                                    duration: "52m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=ca74f8ff4f3d4aaaa33caeff00a56a34"
                                }, {
                                    lid: 3,
                                    lecture_name: "arkivuttrekk.xml",
                                    lecture_description: "arkivuttrekk inneholder mye proveniensinformasjon og er en overordnet beskrivelse av innhold på et 'meta'-nivå.",
                                    lecture_comment: "I opptaket står det at info.xml også blir gjennomgått. info.xml blir håndtert i egen video.",
                                    duration: "50m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=2f975ad65ea544f8aa19af00010db361"
                                }, {
                                    lid: 4,
                                    lecture_name: "arkivstruktur.xml - intro",
                                    lecture_description: "Hvilken strukturer kan være i arkivstruktur.xml og litt om bruken. Overorndet intro.",
                                    lecture_comment: "",
                                    duration: "36m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=57f060d284d44287b911aefd014c923b"
                                }, {
                                    lid: 5,
                                    lecture_name: "arkivstruktur.xml - XSD og arkiv",
                                    lecture_description: "Vi ser hvordan arkivstruktur.xsd brukes og jobber med å utvikle arkivstruktur. Tar arkiv arkivenheten.",
                                    lecture_comment: "",
                                    duration: "43m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=b611e232dd014a209374af0100840d48"
                                }, {
                                    lid: 6,
                                    lecture_name: "arkivstruktur.xml - arkivskaper, arkivdel, klassifikasjonsystem og klasse",
                                    lecture_description: "Fortsetter utvikling av arkivstruktur med overnevnte arkivenheter",
                                    lecture_comment: "",
                                    duration: "39",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=96a967bc7a5c4335a005af0001670fc4"
                                }, {
                                    lid: 7,
                                    lecture_name: "arkivstruktur.xml - mappe og registrering",
                                    lecture_description: "Fortsetter utvikling av arkivstruktur med overnevnte arkivenheter. Vi ser også på hvordan mappe utvides til saksmappe og regsitrering til journalpost. Vi gjør første validering av XML mot XSD.",
                                    lecture_comment: "",
                                    duration: "47m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=429ff6d4e51245b19f48af0200d6d342"
                                }, {
                                    lid: 8,
                                    lecture_name: "arkivstruktur.xml - dokumentbeskrivelse og dokumentobjekt",
                                    lecture_description: "Kommer til slutten av arkivstruktur med dokumentkonteksten",
                                    lecture_comment: "",
                                    duration: "33m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=01907404c8e34a81a69caef70189975b"
                                }, {
                                    lid: 9,
                                    lecture_name: "endringslogg.xml - oversikt",
                                    lecture_description: "endringsloggen inneholder historikken/utviklingen til bestemte elemenet som finnes i arkivstrukturen. Det kan anses som proveniensinformasjon, men kan også være en del av autentisiteten.",
                                    lecture_comment: "",
                                    duration: "22m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=21f7dab95ae94f668313aeff00359f7c"
                                }, {
                                    lid: 10,
                                    lecture_name: "journalrapportene",
                                    lecture_description: "offentlig- og løpendejournal er en slags visning over innholdet i arkivstruktur der fokuset er på journalposten framfor arkivstruktur.",
                                    lecture_comment: "",
                                    duration: "22m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=0209789219b84bcdb818af03005eb8cf"
                                }, {
                                    lid: 11,
                                    lecture_name: "virksomhetsspesifikkeMetadata",
                                    lecture_description: "Merk: Dette er ikke så relevant for eksamen. Bruken av virksomhetsspesifikkeMetadata gjør det mulig å ta i bruk metadata som ikke er en del av arkivstruktur. Det er et veldig nyttig verktøy for dokumentasjonsforvaltning.",
                                    lecture_comment: "",
                                    duration: "19m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=d552167d083e473090a4aefd00a3dc09"
                                }, {
                                    lid: 12,
                                    lecture_name: "arkivdokumenter",
                                    lecture_description: "Litt om arkivdokumenter som inngår i uttrekket.",
                                    lecture_comment: "",
                                    duration: "20m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=888a5581d3464cb7ba23aefc017496a6"
                                }, {
                                    lid: 13,
                                    lecture_name: "info.xml",
                                    lecture_description: "info.xml er siste instans av et interessant tilnærming til hvordan autentisitet blir håndtert.",
                                    lecture_comment: "",
                                    duration: "25m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=06235b658a4d4b18a208af010169db21"
                                }, {
                                    lid: 14,
                                    lecture_name: "Validering av uttrekk",
                                    lecture_description: "Oppsummerer et Noark 5 uttrekk",
                                    lecture_comment: "",
                                    duration: "15m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=92e8546805e24b3da0dbaef9005ab52b"
                                }, {
                                    lid: 15,
                                    lecture_name: "Oppsumering",
                                    lecture_description: "Oppsummerering av Noark 5 uttrekk",
                                    lecture_comment: "",
                                    duration: "29m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=9fda361668884d2c8dceaef7007df409"
                                }
                            ]
                        },
                        {
                            sub_module_id: 3,
                            sub_module_name: "Tabelluttrekk",
                            lectures: [
                                {
                                    lid: 1,
                                    lecture_name: "Bevaring av dokumentasjon",
                                    lecture_description: "Hvor enkelt er det å bevare dokumentasjon, spesielt når det er lagret som tabeller. Vi møter noe av det vi har sett på tidligere og bruker det som et utgangspunkt for tabelluttrekk",
                                    lecture_comment: "",
                                    duration: "42m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=566ee4db7e3a41cd807aaef600362129"
                                }, {
                                    lid: 2,
                                    lecture_name: "Hvordan migreres data til XML",
                                    lecture_description: "Her ser vi hvordan vi rent praktisk kan trekke data fra en tabell og markere det som XML. Vi ser på to tilnærminger, tabell og semantisk",
                                    lecture_comment: "",
                                    duration: "17m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=b1fc9fb5880a4fe49a55aeff0155c185"
                                }, {
                                    lid: 3,
                                    lecture_name: "Hvordan beskrive database egenskaper ",
                                    lecture_description: "Databasegenskaper som feks primær- og fremmednøkler må beskrives med tabelluttrekk. Her ser vi en enkel måte å gjøre det. Vi oppsummerer denne strategien her også",
                                    lecture_comment: "",
                                    duration: "16m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=f050426216b749dda328af000005cf3e"
                                }, {
                                    lid: 4,
                                    lecture_name: "Siard Intro",
                                    lecture_description: "Siard er en format for å lagre data i en relasjonstabell som XML. Det er nyttig at det finnes en standard som gjør dette, men det er mye verdi at SIARD prøver å gjøre uttrekkene uavhengig av databaseleverandører",
                                    lecture_comment: "",
                                    duration: "20m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=0ec11c5bf1434b789f91aefd01335069"
                                }, {
                                    lid: 5,
                                    lecture_name: "Fra database til Siard fil",
                                    lecture_description: "Hvordan dbtk brukes for å trekke database ut fra en database. Hva kan dbtk tilby av innsyn",
                                    lecture_comment: "",
                                    duration: "24m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=6259df7a16c747908152af02011c882b"
                                }, {
                                    lid: 6,
                                    lecture_name: "Fra database til Siard fil ... igjen",
                                    lecture_description: "Samme innhold som Fra database til Siard fil opptaket, bare at vi her går gjennom prossesen på ektevis",
                                    lecture_comment: "",
                                    duration: "13m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=13798ea0fec74cb9aa1baeff01576c5b"
                                }, {
                                    lid: 7,
                                    lecture_name: "Fra Siard til database",
                                    lecture_description: "Her ser vi hvordan vi kan eksportere data fra Siard filen tilbake til en database",
                                    lecture_comment: "",
                                    duration: "10m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=81dfacd7f67d445bb976af03005399b6"
                                }, {
                                    lid: 8,
                                    lecture_name: "Innhold i Siard filen",
                                    lecture_description: "Vi pakker ut siard filen og ser på innholdet, hvordan innhold og databaseteknisk metadata er lagret",
                                    lecture_comment: "",
                                    duration: "18m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=335c76dc3b7a4cc8b7c2aeff00491e41"
                                }, {
                                    lid: 9,
                                    lecture_name: "Oppsummering Siard",
                                    lecture_description: "Oppsumering av det som er gjennomgått",
                                    lecture_comment: "",
                                    duration: "19m",
                                    href: "https://oslomet.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=f3bbf1eb7cc046998116af03003043b7"
                                }
                            ]
                        }
                    ]
                }
            ]
        ;

        $scope.modules = moduleData;

        $scope.doShowModuleContents = function (module) {
            $scope.currentModule = module;
            $scope.currentLecture = module.sub_modules[0].lectures[0];
            console.log(JSON.stringify(module));
            $('#courseModalDir').modal('show');
        };

        $scope.doShowLectureContents = function (lecture) {
            $scope.currentLecture = lecture;
            console.log(JSON.stringify(lecture));
        };
    }])
;




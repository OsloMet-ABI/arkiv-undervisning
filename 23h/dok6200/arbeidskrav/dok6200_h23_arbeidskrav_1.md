## DOK6200-H23 Arbeidskrav 1

Frist: 2023-09-24

**Bokmål**:

Oppgaven leveres i Canvas. Dokumentet skal være i PDF / A-format. Du trenger ikke noen spesielle verktøy for å tegne et ER-diagram, du kan tegne det for hånd og skanne det inn. MS Powerpoint / LibreOffice Impress kan også brukes til å lage ER-diagrammer. Det finnes også gratis online verktøy som kan brukes til å lage ER-diagrammer (f.eks https://www.draw.io/). Du kan bruke phpmyadmin på bibin for å verifisere database kommandoene.

ER-modellering

En venn av deg har spurt deg om du kan se nærmere på datamodelleringsbehovene til sitt nye prosjekt, et selskap som vil selge utvalgte stelleprodukter for kaniner. Ideen går ut på å sette sammen forskjellige og unike kombinasjoner av ulike stelleprodukter som vil hjelpe kanineiere stelle kaninene sine på en slik måte at det er både avslappende og underholdende for alle involverte, samtidig som det øker tillit og nærhet mellom kanin og eier. Selskapet vil bli kalt "The rabbit grooming lounge" og etter en del diskusjon har daglig leder sendt deg følgende beskrivelse som hun tror kan brukes som grunnlag for en ER-modell.

_"The rabbit grooming lounge" ser en forretningsmulighet i å kjøpe stelleprodukter fra aliexpress.com og sette sammen produktene i ulike stellepakker som skal selges via vårt innovative nettsted. Vi starter med med tre pakker, “The bunny wash and go”, “Graciously groomed rabbit” og “rabbit soother”, men vi vil trolig legge til flere sett etterhvert. En pakke vil bestå av ulike verktøy som en saks, kam, massasjeverktøy osv. Kunder kan kjøpe flere sett når de bestiller gjennom vårt innovative nettsted. Selv om produktene våre vil være flotte, de beste produktene, vinnende produkter, så vil vi muligens gjennomføre direkte markedsføring av våre vinnende produkter mot kunder i fremtiden. Vi vil nok ikke trenge å gjøre det da våre produkter er så flotte i utgangspunktet. Uansett ønsker vi å registrere kundeinformasjon og eventuelle kjøp de måtte ha gjennomført. Vi vil annonsere våre pakker i den ukentlige "Rabbits digest". Faktureringsprosessen håndteres av våre regnskapsførere, mens ordrebehandling fra aliexpress.com håndteres av et annet system._

Oppgaver:

1.  Lag ER-diagrammet for databasen og forklar resonnementet bak løsningen din.
2.  Lag det logiske skjemaet for databasen.
3.  Hvilken normalform er løsningen i? Forklar hvorfor.
4.  Lag DDL-komandoer på bakgrunn av det logiske skjemaet.
5.  Lag DML-kommandoer som viser hvordan data settes inn i hvert tabell (maks 3 tupler per tabell).
6.  Lag en SQL-spørring som viser  alle produkter som tilhører de forskjellige pakkene
7.  Lag en eller flere SQL-spørringer som viser en liste over kunder og settene de kjøpte

Dersom du finner noe uklart i beskrivelsen over, skriver du en kommentar om eventuelle forutsetninger du har gjort. Når du lager det logisk skjemaet, skal  primærnøkler understrekes, mens fremmednøkler merkes med en asterisk (*).

**Nynorsk**:

Oppgåva blir levert i Canvas. Dokumentet skal vera i PDF / A-format. Du treng ikkje nokon spesielle verktøy for å teikna eit ER-diagram, du kan teikna det for hand og skanna det inn. MS Powerpoint / LibreOffice Impress kan òg brukast til å laga ER-diagram. Det finst òg gratis online verktøy som kan brukast til å laga ER-diagram (f.eks http://www.draw.io/). Du kan bruka phpmyadmin på bibin for å verifisera database kommandoene.

ER-modellering

Ein ven av deg har spurt deg om du kan sjå nærare på datamodelleringsbehovene til dei nye prosjekta sitt, eit selskap som vil selja utvalde stelleprodukter for kaninar. Ida går ut på å setja saman ulike og unike kombinasjonar av ulike stelleprodukter som vil hjelpa kanineigarar stella kaninane sine på ein slik måte at det er både avslappande og underhaldande for alle involverte, samstundes som det aukar tillit og nærleik mellom kanin og eig. Selskapet vil kallast "The rabbit grooming lounge" og etter ein del diskusjon har dagleg leiar sendt deg følgjande skildring som ho trur kan brukast som grunnlag for ein ER-modell.

_"The rabbit grooming lounge" ser ein forretningsmogelegheit i å kjøpa stelleprodukter frå aliexpress.com og setja saman produkta i ulike stellepakker som skal seljast via den innovative nettstaden vår. Vi startar med med tre pakkar, “The bunny wash and go”, “Graciously groomed rabbit” og “rabbit soother”, men vi vil truleg leggja til fleire sett etterkvart. Ein pakke vil bestå av ulike verktøy som ein saks, kam, massasjeverktøy osb. Kundar kan kjøpa fleire sett når dei bestiller gjennom den innovative nettstaden vår. Sjølv om produkta våre vil vera flotte, dei beste produkta, vinnande produkt, så vil vi moglegvis gjennomføra direkte marknadsføring av dei vinnande produkta våre mot kundar i framtida. Vi vil nok ikkje trenga å gjera det då produkta våre er så flotte i utgangspunktet. Uansett ønske vi å registrera kundeinformasjon og eventuelle kjøp dei måtte ha gjennomført. Vi vil annonsera pakkane våre i den vekentlege "Rabbits digest". Faktureringsprosessen vert handtert av rekneskapsførarane våre, medan ordrehandsaming frå aliexpress.com vert handtert av eit anna system._

Oppgåver:

1.  Lag ER-diagrammet for databasen og forklar resonnementet bak løysinga di.
2.  Lag det logiske skjemaet for databasen.
3.  Kva for ei normalform er løysinga i? Forklar kvifor.
4.  Lag DDL-komandoar på bakgrunn av det logiske skjemaet.
5.  Lag DML-kommandoar som viser korleis data vert sett inn i kvar tabell (maks 3 tupler per tabell).
6.  Lag ein SQL-spørjing som viser alle produkt som tilhøyrer dei ulike pakkane
7.  Lag ein eller fleire SQL-spørjingar som viser ei liste over kundar og setta dei kjøpte
    
Dersom du synest noko er uklart i skildringa over, skriv du ein kommentar om eventuelle føresetnader du har lagt til grunn. Når du lagar det logiske skjemaet, skal primærnøklar understrekast, medan framandnøklar vert merkte med ein asterisk (*).

= ER-modellering
:revealjsdir: ../../../node_modules/reveal.js
:author: Thomas Sødring <tsodring@oslomet.no>
:date: 2022-09-08
:revealjs_theme: white
:revealjs_history: true
:revealjs_fragmentInURL: true
:revealjs_width: 1408
:revealjs_height: 792
:revealjs_controls: true
:revealjs_controlsLayout: edges
:revealjs_controlsTutorial: true
:revealjs_slideNumber: c/t
:revealjs_showSlideNumber: all
:docinfo: shared-head,shared-footer
:imagesdir: ./images/
:favicon: ../../../favicon.ico
:iconfont-remote!:
:iconfont-name: fonts/fontawesome/css/all

ARK2500 - Digital dokumentasjonsforvaltning - 2022

== ER Modellering

* ER Modellering står for «Entity Relationship» eller på norsk «Entitets 
Sammenhenger» 
* Det brukes når du ønsker å utvikle en database som lagrer data for en bestemt 
scenario 
* Det kan brukes til å avgrense og resonere om scenarioet
* Det er en konseptuell modellerings teknikk som definerer entiteter (en del av 
en scenario, konsepter eller ting) og sammenhengene mellom disse konseptene
* Det er en prosess, som brukes til å skape en datamodell for et gitt 
problemområde

===  ER Modellering
* Konseptuell datamodell uten referanse til teknologi
* Ikke utelukkende for å utvikle databaser, men vi ser på det for database 
utvikling 
* Det kan modellere informasjonsbehovet til en organisasjon eller for et gitt 
prosjekt
* Sluttproduktet av  datamodellerings prosessen er et relasjonsdatabase skjema
* Bruker en top-down tilnærming 

== Hvorfor modellerer vi

* «Billigere» å finne feil i en modell  
* En god og korrekt representasjon av data er  viktig for at:
** Databasen skal støtte ønskede operasjoner og forespørsler
** Forenkle endringer eller ønsker til funksjonalitet 
* ER-modellen kan fungere som en del av systemdokumentasjon
** Nyttig for videre arbeid / integrasjoner


== En god konseptuell datamodell

* Avspeiler alle relevante aspekter av «scenarioet» 
* Lettforståelig og utvetydig selv for de med begrenset teknisk forståelse
* Kan systematisk oversettes til en konkret logisk datamodell, som er 
utgangspunktet for en faktisk database implementasjon


==  Hvordan modellerer vi
* Begynner med en analyse av «scenarioet» man ønsker å modellere og beskrive 
det. 
* Beskrivelsen må være:
** Deklarativt - hva, ikke hvordan
** Presis - klar, innforstått mening
** Atomær - en uttalelse
** Konsekvent - internt og eksternt
** Uttrykkbar - strukturert, naturlig språk
** Entydig - ikke-redundans
** prosess-orientert 

== Hvordan modellerer vi

* Veldig mye en iterativt prosess
* Du utvikler mange kladd
* En tavle er veldig nyttig når du er nybegynner
** Kladden gjøres om senere i programverktøy
* Med erfaring jobber du rett på PC
** Profesjonell ER-Diagram utvikling programvare
*** Database strukturen lages fra modellen
* Det finnes gratis online programmer som kan også brukesfootnote:drawio[
https://www.draw.io] 

== Hvordan modellerer vi
* Du må utarbeide en beskrivelse av scenarioet ditt
* Du må tenke konseptuelt
* Kan være greit å ha med «vaktmesteren» eller «kokken»
** Ofte når man prøver å forklare noe / et problem til noen (som du vet ikke 
forstår problemet) så trigger det nye tanker 
** Nyttig hvis 
*** Du låser deg i et spor
*** Du kommer ikke videre

== Hva er en entitet? 
* En entitet representerer en type (konseptuell) objekt ikke en fysisk ting 
** «Bil»
*** ikke «VW Golf» eller «Toyota Yaris» 
** «Lufthavn» 
*** ikke, «Gardermoen» eller «Flesland»
** «Bygning»
*** ikke «P48» eller «P52»
* Objektet kan være representert med kun én entitet
* En entitet må være entydig identifiserbar 

== En entitet 

image::entity.svg[En entitet]

== Entitetens navn 

image::entity_name.svg[Entitet med navn]

== Entitet med attributter 

image::entity_attributes.svg[Entitet med attributter]

== Eksempler på entiteter

image::multiple_entites.svg[Flere entiteter]

== Entitet Eksempel : Bil

image::car_entity_table.svg[Bil entitet med tabell]

== Entitet Eksempel :  Flyplass

image::airport_entity_table.svg[Flyplass entitet med tabell]

== Entitet Eksempel : Bygning

image::building_entity_table.svg[Bygning entitet med tabell]

== Sammenhengstyper

image::relationship_types.svg[Sammenhengstyper]

== Hva betyr én-til-én

* En én-til-én sammenheng mellom to tabeller (Tabell A og Tabell B) oppstår når
  hver rad i Tabell A kan kun ha én relatert rad i Tabell B 

image::one_to_one_table.svg[En til en]

===  Hva betyr én-til-én

* Altså er dette er 'ulovlig' med en én-til-én sammenheng

image::one_to_one_table_not_possible.svg[Ikke mulig i en en-til-en]

== Hva betyr én-til-mange

* Én-til-mange sammenhenger oppstår når hvert enkelt rad i Tabell A kan ha 
 mange relaterte rader i Tabell B men hver rad i Tabell B har bare en relatert 
rad i Tabell A


image::one_to_many_table.svg[En til mange]


== Hva betyr mange-til-mange

* Mange-til-mange sammenhenger oppstår når hver rad i Tabell A kan ha mange 
 relaterte rader i Tabell B og hver rad i Tabell B kan ha mange relaterte rader 
 i Tabell A

image::many_to_many_table.svg[Mange til mange som ikke kan realiseres i en relasjonsdatabase]

== Mange-til-mange sammenhenger

* En mange-til-mange sammenheng må løses før databasen implementeres
** M:m kan ikke realiseres i en relasjonsdatabase
* Kan illustrere et område med dårlig forståelse 
* En mange-til-mange sammenheng skjuler en skjult entitet 
* Håndteres ved å identifisere og oppløse den opprinnelige mange-til-mange til 
to én-til-mange relasjoner 

== Mange-til-mange koblingstabell

image::many_to_many_link_table.svg[Mange-til-mange koblingstabell]

== Hvordan komme i gang?

* Du skal prøve å identifisere entiteter fra en beskrivelse av en 
forretningsdomene
** Veldig iterativt og kverulerende
* Du jobber på «whiteboard» og tegner entiteter og prøver å identifisere 
sammenhengene mellom de
* Identifisere attributter som gjelder for entitetene 
** feks ISBN, tittel for bok
* Kverulerer mye med deg selv eller andre i gruppen
* Iterativt prosess, kast og begynn på nytt

== Eksempel

[.text-left]
[.small]
Du er blitt engasjert som databasedesigner av "The Vidda Coffee Import Company",
 som skal drive med import av kaffe for videresalg til kunder i Norge.  Etter 
noen diskusjonsrunder med ledelsen,  har du kommet fram til følgende 
beskrivelse av forretningmodellen:

[.text-left]
[.small]

"The Vidda Coffee Import Company"  vil importere forskjellige kaffesorter fra 
ulike *land*. Det er gjort avtaler med Etiopia, Brasil og  Ecuador, men det 
forventes  import fra flere land etterhvert. Databasen må lagre informasjon om 
de forskjellige *leverandørene* og hva slags *kaffe* (mengder / sorter osv.) 
som er *bestilt*. Det kan være flere leverandører i samme land.


[.text-left]
[.small]
Firmaet kommer i begynnelsen  kun til å levere kaffe til kunder i Norge. Det er
 mulig at det etter  hvert også vil levere til utlandet. I tillegg til 
informasjon om kunder, vil det lagres informasjon om bestillingene.  

[.text-left]
[.small]
FN har vedtatt bruken av forskjellige sertifiseringssystemer av 
kaffeleverandører. Leverandører som møter kravene fra FN om ordentlig lønn og 
arbeidsvilkår  blir sertifisert som "fair trade". De som ikke imøtekommer et 
minstekrav får status som "ingen-sertifisering". Noen leverandører kan bli 
svartelistet som "unfair trade". Denne sertifiseringmodellen kommer til å bli 
utvidet etter hvert. Databasen skal kunne koble sertifiseringene til 
leverandørene.  

=== Eksempel

1. Lag ER diagrammet for databasen 
2. Lag det logiske skjemaet for databasen
3. Hvilken normalform er løsningen i? Forklar hvorfor.
4. Lag DDL-komandoer på bakgrunn av det logiske skjemaet.
5. Lag DML-kommandoer som viser hvordan data settes inn i hvert tabell
(maks 3 tupler per tabell).
6. SQL spørring

[.text-left]
__Dersom du finner noe uklart i beskrivelsen over, skriver du en kommentar om 
eventuelle forutsettninger du har gjordt. Primærnøkkler understrekes, 
fremmednøkkler merkes med en *__

== Oppsummering

* Prosessen blir veldig individuell basert på kunnskap og erfaring, men 
hovedtrekket er 
** Beskrive «systemet» vi skal modellere
** Utifra beskrivelse definerer vi entiteter og multiplisiteten i relasjonene 
mellom dem
** Identifiser attributtene til entitetene
* Større «systemer» vil se en del iterasjoner og flere forslag blir forkastet 
før man kommer fram til en løsningen
* Jobb med små deler av problemet om gangen og bygg løsningen ut
* Skal ikke løse alt på en gang

== UML klasse diagram / ER modell

* En UML klassediagram ligner på en ER modell
* UML er et __språk__ for å designe et programvaresystem og har flere 
bruksområder
** System modellering 
** Vise interaksjon
* ER diagramer brukes for å resonere om en datamodell og sammenhengene mellom
 entiter
** Nyttig for database utvikling, men også XML
* En UML klassediagram ligner litt på en ER modell


=== UML klasse diagram 

image::uml-arkivstruktur-diagram.png[UML beskrivelse av arkivstruktur]

[.columns]
== PlantUMLfootnote:plantuml[https://plantuml.com/]

Se også footnote:planttext[https://www.planttext.com/]

[.column]
[source,linenumbers]
class Arkivstruktur.Registrering <Arkivenhet> {
  +arkivertDato : datetime [0..1]
  +arkivertAv : string [0..1]
  +referanseArkivertAv : SystemID [0..1]
  +kassasjon : Kassasjon [0..1]
  +skjerming : Skjerming [0..1]
  +gradering : Gradering [0..1]
  +referanseArkivdel : SystemID [0..1]
  +registreringsID : string [0..1]
  +tittel : string
  +offentligTittel : string [0..1]
  +beskrivelse : string [0..1]
  +noekkelord : string [0..*]
  +forfatter : string [0..*]
  +dokumentmedium : Dokumentmedium [0..1]
  +oppbevaringssted : string [0..*]
  +virksomhetsspesifikkeMetadata : any [0..1]
}

[.column]

image::record_class_diagram.svg[Registrering som klassediagram (entiteter)]


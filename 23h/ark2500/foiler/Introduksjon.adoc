
== ARK2500-H22

Velkommen

ARK2500

Digital dokumentasjonsforvaltning

Høst 2022

Thomas Sødring

tsodring@oslomet.no

P48-R407

67238287


== Kontakt info

Thomas Sødring

* Epost: tsodring@oslomet.no
* Hvis du ikke får svar innen 48 timer, send mailen igjen!!!!
* Rom: P48-R407
* Telefon: 67238287


== Fagside

* Benytter en fagside for å tilgjenliggjøre emnets innhold
* Viktig at informasjonsarbeider kan jobbe utenfor siloer
** https://oslomet-abi.gitlab.io/arkiv-undervisning/22h/ark2500/ark2500.html
*Canvas brukes for å holde kontakt / formidle meldinger




== Hva dette emnet handler om

*Digital (elektronisk, ikke papir)
*Dokumentasjonsforvaltning handler om å
** forvalte dokumentasjon fra det øyeblikket det er skapt og gjennom aktiv bruk til hvordan det skal bevares i en langtidsperspektiv samtidig som forståelighet og autentisitet er ivaretatt



== Emnet

__Emnet kombinerer teori og praksis for å bygge opp en forståelse av digital dokumentasjonsforvaltning. Det tar for seg sammenhengene mellom nyere arkivteori og digital dokumentasjonspraksis med vekt på relevante standarder. Det legges vekt på datamodellering og hvordan databaser brukes under dokumentasjonsprosessen.__



== Emnet

Profesjonen gjennomgår en endring og det er usikkert hvor vi skal

image::Images/1779847527-image1.png[]
Gjengitt fra:

https://www.nrk.no/norge/viktige-dokument-blir-ikkje-arkivert-_-det-kan-fa-store-konsekvensar-1.15604824



== Om Noark

Arkivverket har sagt at de avslutter arbeidet med videreutvikling av Noark

Noark blir med oss framover en stund og uansett vil standarden kunne være en case-studie om en __best-practice__ som gir god innsikt i dokumentasjonsforvaltning

Mye av kritikken Noark blir utsatt for handler mest om uvitenhet


== Kunnskaper

god forståelse av arkiv- og dokumentasjonsbegrepet i en digital kontekst.

grunnleggende kunnskap om databaseteori og datamodellering.

god kunnskap om oppbygging og funksjoner i NOARK 5-standarden.


== Ferdigheter

anvende sentrale teoretiske og metodologiske begreper knyttet til danning av digitale arkiver

strukturere enkle datamodeller og databaser og utføre enkle spørringer mot en database

anvende relevante nasjonale/internasjonale IKT-, arkiv- og dokumentasjonsforvaltningsstandarder


== Blir jeg en dataingeniør?

Nei, men en dataingeniør kunne også lært noe nyttig her

Dere vil lære om IT-konsepter fra en dokumentasjonsforvaltningsperspektiv

Man kan ikke ignorere IT-siden, men hvordan lærer man å snakke IT

Blanding av teori og praktisk arbeid

Mye vekt på det praktiske

* øvelse gjør mester


== Organisering 

Arbeidsformene er nettbaserte forelesninger, nettbaserte diskusjoner, oppgaveløsning og selvstudium

https://tp.uio.no/oslomet/timeplan/timeplan.php?type=course&id[]=ARK2500%2C1&sort=null

Undervisning

* Hver mandag 08:30-12:15

Vi får studentressurser som skal hjelpe


== DOK6200

Emnet har mange paralleller med DOK6200

Dere kan gjerne også se på forelesningene i disse emnene

* https://oslomet-abi.gitlab.io/arkiv-undervisning/22h/dok6200/dok6200.html

DOK6200 opptak er mer «konsentrert» mens ARK2500/2510 er mer forelesning


== Arbeidskrav 

Emnet har to arbeidskrav

* ER-modellering (frist 2022-10-02)

* Noark 5 i database (frist 2022-10-30)

Arbeidskrav tilbakemelding er bestått / ikke bestått

* Dersom det er behov for oppfølging vil lærer kunne gi mer tilbakemelding

Arbeidskrav kan brukes som utgangspunkt til veiledning


== Vurdering

Studentenes læringsutbytte vil bli vurdert ved en skriftlig mappe med et omfang på 10-13 sider (23.000 - 30.000 tegn inkludert mellomrom).

Skrifttype og skriftstørrelse: Arial / Calibri 12pkt. Linjeavstand: 1,5.


== Nasjonal karakterskalafootnote:[https://www.uhr.no/temasider/karaktersystemet/karakterbeskrivelser/]

[options="header"]
|=========================================================================================
| symbol | betegnelse    | generell, ikke fagspesifikk beskrivelse av vurderingskriterier        
| A      | fremragende   | Fremragende prestasjon som klart utmerker seg. Kandidaten viser svært god vurderingsevne og stor grad av selvstendighet.                                          
| B      | meget god     | Meget god prestasjon. Kandidaten viser meget god vurderingsevne og selvstendighet.      
| C      | god           | Jevnt god prestasjon som er tilfredsstillende på de fleste områder. Kandidaten viser god vurderingsevne og selvstendighet på de viktigste områdene.                              
| D      | nokså god     | En akseptabel prestasjon med noen vesentlige mangler. Kandidaten viser en viss grad av vurderingsevne og selvstendighet.                                                       
| E      | tilstrekkelig | Prestasjonen tilfredsstiller minimumskravene, men heller ikke mer. Kandidaten viser lite vurderingsevne og selvstendighet.                                                       
| F      | ikke bestått  | Prestasjon som ikke tilfredsstiller de faglige minimumskravene. Kandidaten viser både manglende vurderingsevne og selvstendighet.                                             
|=========================================================================================




== Om foiler

Noen foiler kan være i ODP (OpenOffice/LibreOffice) format

* http://www.libreoffice.org/

Noen er ren html generert fra asciidoc, men det skal være PDF tilgjenglig også.

Prøver å få til en ren datadrevet emne der kilden til alt er tekstlige formater


== !

**__Lykke til!__**


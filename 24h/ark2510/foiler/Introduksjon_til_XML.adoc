= Introduksjon til XML
:revealjsdir: ../../../node_modules/reveal.js
:author: Thomas Sødring <tsodring@oslomet.no>
:date: 2024-08-31
:revealjs_theme: white
:revealjs_history: true
:revealjs_fragmentInURL: true
:revealjs_width: 1408
:revealjs_height: 900
:revealjs_controls: true
:revealjs_controlsLayout: edges
:revealjs_controlsTutorial: true
:revealjs_slideNumber: c/t
:revealjs_showSlideNumber: all
:docinfo: shared-head,shared-footer
:imagesdir: ./images/
:favicon: ../../../favicon.ico
:iconfont-remote!:
:iconfont-name: fonts/fontawesome/css/all

ARK2510 - Migrasjon av dokumentasjon - 2024

== Dagens økt

Vi skal først gå kort gjennom tre måter å gjøre datauttrekk på som er i bruk i 
dag, men med fokus på XML.footnote:gjennomgang[_Gjennomgangen kommer til å vise 
hvorfor dette er det mest hensiktsmessige formatet å fokusere på og dermed også 
å jobbe praktisk med_.] Vi skal deretter jobbe praktisk med å lage fire ulike 
XML-filer

=== Følgende skal utvikles

* navn.xml
* person.xml
* personListe.xml
* bok.xml

_Alle skal ha disse fire filene lagret i en mappe innen slutten av uka!_

== Uttrekksprosessen

image::from_records_to_preservation.png[Fra danning til bevaring]

== Hvordan lage datauttrekk fra en database?

* Data er lagret i relasjoner/tabeller i en database
* Må trekkes ut og lagres i et 'nøytralt' format
* Hvilke metoder har vi for dette?
** 'Fixed-width' filer
** Verdier separert med komma
** Markup språk, i vårt tilfelle XML


== Hva er ‘fixed-width’?

* Fixed-with eller fastform er en måte å lagre data i en fil der hver linje 
inneholder en avgrenset mengde med informasjon (tenk rad av informasjon fra en
 tabell i en database) og hvert felt har en bestemt lengde (antall tegn)
** Et felt tilsvarer en kolonne avgrensning

=== Hva er ‘fixed-width’?

image::fixed_width_cars_table.png[Bildata vist som fastlengde]

== Problemer med ‘fixed-width’

* Dersom data er oppdatert slik at width-avgrensningene ikke passer lenger, får
 programmene som skal lese denne typen filer problemer
* Avhengighet mellom data, grensene mellom data (data boundaries) og programmet
 som leser data
* Dette kan kanskje håndteres med en prolog på toppen av fila for å beskrive 
widths eller kanskje med et versjonsnummer av strukturen
* Men avhengighetsfaktoren skaper uansett problemer!

=== ‘fixed-width’-fil
image::fixed_width.png[Innhold med fastlengde]

=== ‘fixed-width’ problemer
image::fixed_width_problems.png[Innhold med fastlengde med avgrensning feil]

== CSV - Verdier separert med komma

* ...eller ‘comma separated values’ som er en måte å avgrense data på, normalt 
 ved hjelp av kommategnet. (Data og verdier betyr altså her det samme)

[source,csv]
----
10001, Pål, Solberg, Storgata, 4, 0182, Oslo
----


* Hver rad fra databasen korresponderer med en rad i csv-fila
* Hvert felt korresponderer med en gitt kolonnefootnote:csveksempel[Merk at i 
dette eksempelet er det ingen markør/avgrensning for data utenom kommaet i seg 
selv. Vi kommer tilbake til markører for data om et par eksempler]

== Eksempel CSV

* Hver linje representerer en person:

[source,csv,linenums]
----
10001, Pål, Solberg, Storgata, 4, 0182, Oslo
10002, Thomas, Hansen, Bakken, 8b, 1406, Ski
10003, Eli, Rørvik, Saturnringen, 47, 1808, Askim
10004, Børre, Andersen, Bekkefaret, 5, 0348, Oslo
----

== Eksempel CSV

* Med litt dokumenterende metadata:

[source,csv,linenums]
----
– ID, fornavn, etternavn, adresse, husnummer, postnummer, by
– start
10001, Pål, Solberg, Storgata, 4, 0182, Oslo
10002, Thomas, Hansen, Bakken, 8b, 1406, Ski
10003, Eli, Rørvik, Saturnringen, 47, 1808, Askim
10004, Børre, Andersen, Bekkefaret, 5, 0348, Oslo
– stopp
----

== Begrensninger med CSV

* Veldig avhengig av rekkefølgen på strukturen!
* Hva skjer om det forekommer et komma i data?
* Vi kan ha en avgrensning for hvert felt (feks ,) og en avgrensning for data 
 (feks "), så kan data bli ignorert dersom det forekommer et komma inni data.

[source,csv,linenums]
----
“10001”, “Pål”, “Solberg”, “Storgata”, “4”, “0182”, “Oslo”
----

== Begrensninger med CSV

* Det kan være vanskelig å oppdage feil og manglende informasjon i filene. Spesielt dersom store filer er prossessert satsvis.
** Hva skjer da om det forekommer feil?

[source,csv,linenums]
----
10001, Solberg, Pål, Storgata, 4, 0182, Oslo
10002, Thomas, Bakken, 8b, 1406, Ski
10003, Eli, Rørvik, Saturnringen, 47, 1808, Askim
10004, Børre, Andersen, 5, 0348, Oslo
----

[source,csv,linenums]
----
10001, Pål, Solberg, Storgata, 4, 0182, Oslo
10002, Thomas, Hansen, Bakken, 8b, 1406, Ski
10003, Eli, Rørvik, Saturnringen, 47, 1808, Askim
10004, Børre, Andersen, Bekkefaret, 5, 0348, Oslo
----

== Det handler om interoperabilitet!

* De to eksemplene med feil er gyldige, men dårlige:
* Både ‘fixed-width’ og CSV fungerer fint i et kontrollert miljø
* Men når du trenger å utveksle informasjon mellom systemer, kan det bli 
vanskelig å kontrollere kvaliteten på filene
** Vanskelig å håndtere ulike versjoner av filene
* Oppsummert så skjer det hyppig feil med både fixed-width og CSV, nettopp fordi data eksisterer i et økosystem av systemer og/eller fordi de må flyttes mellom systemer i forbindelse med langtidsbevaring

[%notitle]
== XML

Om det bare fantes en måte å tagge data på slik at hvert felt betydde én ting…

Svaret er noe som heter:

XML

== Hva er XML?

* eXtensible Markup Language
* Publisert av W3 Consortium: https://www.w3.org/
* XML er som navnet tilsier et markupspråk som kombinerer tekst samt 
tilleggsinformasjon (altså metadata) om teksten
* XML kan også kalles et metaspråk, siden det brukes for å markere og beskrive 
en gitt (hoved)tekst med tekst
* ...og et XML-dokument er et dokument som inneholder data som er markert på en 
spesifikk måte etter standardiseringen i lenken ovenfor

== Markert datafootnote:xml[Formelt ikke XML syntaks men nær nok]

[source,xml,linenums]
----
<id>10001</id>
<etternavn>Solberg</etternavn>
<adresse>Storgata</adresse>
<fornavn>Pål</fornavn>
<husnummer>4</husnummer>
<postnummer>0182</postnummer>
<by>Oslo</by>
----

== Hva er et markup language?

Begrepet kommer fra prosessen med å markere manuskripter hvor instruksjoner i form av symboler blir lagt til manuskriptet og tolket når manuskriptet skrives ut
Eksempler på markupspråk:

* GenCode from 1960s
* Scribe 1980
* GML
* SGML
* HTML
* XML
* xhtml

== Hvorfor bruke XML til uttrekk?

* Selvbeskrivende
* Interoperabilitet
* Enkelt å utvikle
* Ikke-proprietært

== Hvorfor bruke XML til uttrekk?

 (...i motsetning til fixed width og CSV)

* Rekkefølgen på strukturen i XML-dokumentet er (potensielt) irrelevant og det er 
enkelt å oppdage om det mangler felter. 
* Det kan enkelt legges til nye felt

== XML og arkiv

* Egnet format ved uttrekk for å overføre informasjon til et arkiv for langtidsbevaring for å sikre interoperabilitet: 

* Brukt for overføring av Noark 4/5 og i elektroniske arkivpakker (OAIS/DIAS)

* XML er et viktig standardisert språk både gjennom records management-prosessen
 og for de ulike langtidsbevaringsfasene, nettopp fordi langtidsbevaring i 
 praksis starter allerede med databasedanningen 

== Men er språket alene nok?

== Nei

[source,xml,linenums]
----
<id>10001</id>
<etternavn>Solberg</etternavn>
<adresse>Storgata</adresse>
<fornavn>Pål</fornavn>
<husnummer>4</husnummer>
<postnummer>0182</postnummer>
<by>Oslo</by>
----

== Vi trenger også struktur


== Data / Struktur / Presentasjon

image::data_struktur_presentasjon.png[Data Struktur Presentasjon,width=60%]

== Fra et arkivfaglig perspektiv...

image::data_struktur.png[Data og struktur]

== Et XML-dokument består av

* en **prolog**
* et **rotelement **(kalles også dokumentelement)
* Inni rotelementet kan vi lage elementer
* Inni elementene kan vi lagre kjernedata.
* Alt etter rotelementet i et dokument blir ansett som søppel!
** <root>innhold</root> _søppel_

* Vi kan kalle rotelementet hva vi vil (i likhet med alle de andre elementene vi
lager). 
** Men husk at fornuftige valg av navn på elementene gjør dokumentet intuitivt 
leselig og forståelig!

== XML-dokument

image::xml_document.png[Oppbygningen av et XML-dokument]


== XML-dokument: Prolog

image::xml_document_prolog.png[XML-dokument med prolog]


=== XML-dokument: Prolog

* XML deklarasjon
* Kommentarer 
* Blanke linjer
* Validering av struktur
* Prosesseringsinstruksjoner
** feks. informasjon om stil 

[source,xml,linenums]
----
<?xml version="1.0" encoding="UTF-8"?>
<!-- Dette er en kommentar --> 
<!DOCTYPE arkiv SYSTEM "https://www.oslomet.no/dtd/student.dtd">
<?xml-stylesheet href="student.css" type="text/css"?>
----

== XML-dokument: Rotelement

image::xml_document_root_element.png[XML-dokument med prolog]

=== XML-dokument: Rotelement

[source,xml,linenums]
----
<?xml version="1.0" encoding="UTF-8"?>
<rotElement></rotElement>
----

== Foreldre og barnfootnote:children[Dette blir mye enklere å forstå etter at vi har laget noen XML-dokumenter i praksis!]

* Et rotelement som innholder andre elementer er alltid forelder til disse 
(under)elementene
* Alle elementer kan prinsipielt ha underelementer, altså barn 
* Dette betyr at mange elementer er både forelder og barn
* Rotelementet er imidlertid alltid bare forelder og noen elementer er bare barn
   
== Rotelementet har barn

image::xml_document_root_element_children.png[XML-dokument med prolog]

=== Rotelementet har barn

[source,xml,linenums]
----
<arkiv>
 <arkivdel>
  <mappe>
  </mappe>
 </arkivdel>
</arkiv>
----

== Vi skal nå gå praktisk til verks!

== NetBeans

* Vi bruker NetBeans for å jobbe med XML
** Programmet er tilstrekkelig for å lære seg de grunnleggende prinsippene vi 
 har om i dette introduksjonskurset
** I en arkivinstitusjon brukes ofte proprietær programvare for å jobbe med XML, 
 da disse er mer brukervennlige og innehar en større grad av skalerbarhet

===  Annet relevant verktøy 

* https://www.jetbrains.com/ (Intellij)
* https://xml-copy-editor.sourceforge.io/
* https://www.oxygenxml.com/
* https://vscodium.com/

== Eksempel: NetBeans

image::netbeans.png[Skjermbilde av Netbeans]

== Starter med ...

* Vi skal lage vårt første XML-dokument med en deklarasjon og en kommentar, 
men UTEN rotelementetfootnote:bad_xml[_Merk! Uten rotnode anses ikke innholdet
som et gyldig XML dokument!_]

[source,xml,linenums]
----
<?xml version="1.0" encoding="UTF-8"?>
<!-- Dette er en kommentar -->
----

== Elementer

* Elementer er grunnlaget for å markup av data i XML-filer og består av 
** En startag
** Innhold
** En endetag

=== Elementer

image::xml_tags.png[XML start- og slutttag]

== Elementer

* Når vi omgir data med start- og endetagger ‘markerer’ vi data
* Navnet du velger for taggene bør være beskrivende for innholdet, fordi
poenget med XML er at det skal være selvbeskrivende 
* Dette er en av grunnene til at XML er et egnet format for å bevare data og 
dermed også nyttig for langtidsbevaring

=== Elementer

* Forslag til elementnavn som kan brukes for å beskrive en person?

[source,xml,linenums]
----
<person></person>
<navn></navn>
<alder></alder>
----

=== Eksempel: navn.xml

* Hvor er prologen?
* Hvor er rotelement?
* Hva skjer om vi legger til noe etter </navn>?

[source,xml,linenums]
----
<?xml version="1.0" encoding="UTF-8"?>
<!-- Dette er en kommentar -->
<navn> 
 <fornavn>Hans</fornavn>
 <etternavn>Hansen</etternavn>
</navn>
----

== White spaces i datafootnote:white_space[håndteres annerledes i attributter]

* _'White spaces'_ er et vanlig uttrykk for 
** space, tab, return
* XML-parseren gjør ingenting med _white space_ i data, men lar det derimot være
 opptil applikasjonen å tolke white spaces
* Å håndtere white space kan konfigureres i XSD

=== White spaces i data

[source,xml,linenums]
----
<bok>  
  <navn>Boken om XML</navn>
  <forfatter>Hans Hansen</forfatter>
</bok>
----


[source,xml,linenums]
----
<bok>
  <navn>Boken   om     XML
          </navn>
  <forfatter>  Hans
      Hansen
   </forfatter>
</bok>
----


[source,xml,linenums]
----
<bok>

  <navn>Boken om XML</navn>


  <forfatter>Hans Hansen</forfatter>

</bok>
----


== Hvordan beskrive en person med XML

* Vi skal dokumentere følgende informasjon om en person:
** fornavn
** mellomnavn 
** etternavn
** personnummer
** kjønn


== Eksempel: person.xml

[source,xml,linenums]
----
<?xml version="1.0" encoding="UTF-8"?>
<person> 
 <fornavn>Hans</fornavn>
 <mellomnavn>John</mellomnavn>
 <etternavn>Hansen</etternavn>
 <personnummer>01108298649</personnummer>
 <kjoenn>mann</kjoenn>
</person>
----

== Sammenlignet med data i en database...

* Hvordan forholder dette seg til informasjonen i en database eller til 
ER-modellering? 
** Rotelementet <person> vil typisk tilsvare entitet (i ER-modellering) og ofte korresponderer med navnet på en tabell
** Elementnavnet, feks. <fornavn> korresponderer med attributtene/kolonnene i tabellen/relasjonen. 
** Innholdet i elementene korresponderer tilsvarer radene med data fra en tabell/relasjon
** Se etter parallellene for hvordan data forholder seg i et xml-dokument i forhold til i en database

== Men hva om det dreide seg om mer enn én person?

* Hva skjer om vi forsøker å legge til en <person></person> til i den samme fila?
** Det blir søppel etter rotelementet!

* Da må vi lage en liste:
** <personListe></personListe>
* Klassisk problemstilling som nybegynere sliter med, spesielt når de jobber 
opp mot data i en database

* Senere vil vi lære å resonere og planlegge XML-dokumentet vårt med XSD

== Eksempel: personList.xml
[source,xml,linenums]
----
<?xml version="1.0" encoding="UTF-8"?>
<personListe> 
    <person> 
       <fornavn>Hans</fornavn>
       <mellomnavn>John</mellomnavn>
       <etternavn>Hansen</etternavn>
       <personnummer>01108298649</personnummer>
       <kjoenn>mann</kjoenn>
    </person>
</personListe> 
----

== Sammenlignet med data i en database...

* Litt annerledes nå: Rotelementet tilsvarer ikke lenger entiteten alene, men 
 fungerer nå som en indikasjon på at det finnes flere forekomster av entiteten
** Men det er ikke noe bestemt krav om hva elementer skal hete
* <person> elementet tilsvarer forsatt én entitet, men fungerer nå som en avgrensning mellom flere forekomster av entiteten
* ...eller som en avgrensning mellom radene i tabellen, om du vil

== Å beskrive en bok med XML

* Vi skal beskrive boken konseptuelt
* Grunnleggende egenskapene man kan forvente å finne om en bok:
** tittel
** forfatter
** kapitler
* Et kapittel har ofte elementer som kapittelnavn, avsnitt osv.
* Med alle disse elementene oppstår det et behov for å definere struktur.


== Eksempel: book.xml

[source,xml,linenums]
----
<?xml version="1.0" encoding="UTF-8"?>
<bok> 
 <forfatter>Hans Hansen</forfatter>
 <bokTittel>The bok about XML</bokTittel>
 <kapittel>
   <kapittelTittel>Introduction</kapittelTittel>
   <paragraf></paragraf>
   <paragraf></paragraf>
 </kapittel>
 <kapittel>
   <kapittelTittel>XML Root element</kapittelTittel>
   <paragraf></paragraf>
   <paragraf></paragraf>
 </kapittel>
</bok>
----

== Tomme elementer?

* Elementer som ikke inneholder data er _tomme_
** Tilsvarer _null_ verdi i databasen

Eksempel på tomt element:

[source,xml,linenums]
----
<paragraf></paragraf>
----

Når et element er tomt, kan det skrives slik:

[source,xml,linenums]
----
<paragraf/>
----

* Det bør aldri forekomme tomme elementer i et Noark 5-uttrekk!
** Noark krav 6.4.5 sier at metadataenheter som ikke inneholder noen verdi skal
 holdes utenfor uttrekket 

== Eksempel på tomme elementer

[source,xml,linenums]
----
<?xml version="1.0" encoding="UTF-8"?>
<bok> 
  <forfatter>Hans Hansen</forfatter>
  <bokTittel>The bok about XML</bokTittel>
  <kapittel>
   <kapittelTittel>Introduction</kapittelTittel>
   <paragraf></paragraf>
   <paragraf/>
 </kapittel>
 <kapittel>
   <kapittelTittel>XML Root element</kapittelTittel>
   <paragraf></paragraf>
   <paragraf/>
 </kapittel>
</bok>
----

== To typer elementer:

[source,xml,linenums]
----
<name> 
  <fornavn>Hans</fornavn>
  <etternavn>Hansen</etternavn>
</name>
----

* *simpleType* som kun inneholder data 
** linje 2 og 3
* *complexType* kan inneholde barn 
** altså elementer inni elementet
** linje 1 og 4

== Så langt har vi altså gått igjennom:

* XML som et markeringspråk og sett på 
** Et XML-dokument:
*** Grunnleggende regler
*** Struktur og dokumentelementer
** Elementer 
*** Definisjon og typer
** Identifisert at det er et behov for struktur i dokumentet

== XML hører til en familie med relatert teknologi

* XML
** Markup language
* XSD/DTD
** Definerer struktur og kan bli brukt for validering
* XSLT
** Brukt for å presentere eller endre data
* XPath / XQuery
** Brukt til filtrering/søk

== Prosesseringsinstruksjoner

* Prosesseringsinstruksjoner er ikke en del av XML-dokumentet
** Det henvises til en annen fil
* Gir instruksjoner for hvordan (en ekstern applikasjon) skal kunne prosessere 
et XML-dokument
** Feks. Hvordan konvertere dataene til et annet format
* Starter med <? og slutter med ?>

[source,xml,linenums]
----
<?xml-stylesheet href="arkiv.css" type="text/css"?>
----

* XML-filen formateres i henhold til CSS-format instruksjoner angitt i filen arkiv.css

== Prosesseringsinstruksjoner

* En xslt er en fil som spesifiserer hvordan XML skal prosesseres for å skape et
 nytt dokument
** kan være XML, JSON, HTML osv
* Man trenger en XSLT-prosessor (firefox)

=== Prosesseringsinstruksjoner

image::xslt_processing.png[XSLT prossesering]

== Prosessering av fila arkiv.xml

* link:../ressurser/xslt/arkiv.xml[arkiv.xml^]
* link:../ressurser/xslt/arkiv_med_css.xml[arkiv_med_css_eksempel.xml^]  
* link:../ressurser/xslt/arkiv_til_html.xml[arkiv_xslt_eksempel.xml^]
* link:../ressurser/xslt/arkiv_til_html_2.xml[arkiv_xslt_eksempel_2.xml^]
* link:../ressurser/xslt/xml2html.xslt[xml2html.xslt^] 
* link:../ressurser/xslt/arkiv.css[arkiv.css^]

Merk! Kunnskap om CSS og XSLT er ikke en del av dette emnet. Vi har de bare med
 for å få litt innsikt i XML-stacken 

== Prosesseringsinstruksjoner

* Foreløpig er ikke profesjonen veldig opptatt av dette, men de kommer på et 
tidspunkt til å bli det
* Når et arkiv bruker XML, er det for å prosessere eller å bevare dokumentene?
* Mitt inntrykk er at fagfeltet er veldig avhengig av RDBMS for å prosessere 
data

== Kritikk mot XML

* Unødvendig
* For mye markup av data
* Filene blir større enn nødvendig

[source,xml,linenums]
----
<person> 
<fornavn>Hans</fornavn>
<etternavn>Hansen</etternavn>
<alder>45</alder>
</person>
----
87 tegn

[source,sql,linenums]
----

person ( 
fornavn(Hans)
etternavn(Hansen)
alder(45)
)
----
49 tegn

== Fordeler med XML

* Markup består av ren tekst og er leselig for mennesker
* Data er ikke adskilt fra representasjonen av data
* Uavhengig av system, programvare eller maskinvare
* Ikke-proprietært
* Det er relativt enkelt å implementere løsninger på toppen av XML
* Enkelt å importere eller å eksportere data til en database ved å bruke XML

== XML som arkivformat i Norge

* XML brukes innen bevaring:
** Noark 4 og 5 uttrekk
** DIAS-standarden (det nasjonale arkivpakkeformatet) er basert på XML

* XML brukes innen journalføring
** Export og import i et Noark system
** BEST-standard for utveksling av dokumenter


DROP DATABASE IF EXISTS s123456_film;
CREATE DATABASE s123456_film;
USE s123456_film;

CREATE TABLE person (
 personID INT, 
 fornavn VARCHAR(20), 
 etternavn VARCHAR(20), 
 fodselsdato DATE,
 dodsdato DATE,
 PRIMARY KEY (personID)
);

CREATE TABLE film (
 filmID INT, 
 tittel VARCHAR(20), 
 aar DATE, 
 lengde SMALLINT, 
 produsent VARCHAR(20),
 distributorID INT,
 PRIMARY KEY (filmID)
);

CREATE TABLE distributor (
 distributorID INT, 
 navn VARCHAR(20),
 adresse VARCHAR(20),
 postnr VARCHAR(20),
 sted VARCHAR(20),
 PRIMARY KEY (distributorID)
);

CREATE TABLE kunde (
 kundeNR INT, 
 fornavn VARCHAR(20), 
 etternavn VARCHAR(20), 
 adresse VARCHAR(20), 
 postnr VARCHAR(4), 
 sted VARCHAR(20),
 PRIMARY KEY (kundeNR)
);

CREATE TABLE kinosal (
 salNR INT, 
 kapasitet INT, 
 filmID INT,
 PRIMARY KEY (salNR),
 FOREIGN KEY (filmID) REFERENCES film (filmID)
);

CREATE TABLE deltagelse (
 personID INT, 
 filmID INT, 
 rolle VARCHAR(20),
 PRIMARY KEY (personID, filmID),
 FOREIGN KEY (personID) REFERENCES person (personID),
 FOREIGN KEY (filmID) REFERENCES film (filmID)
);

CREATE TABLE bestilling (
 kundeNR INT, 
 filmID INT, 
 dato DATE, 
 tid TIME, 
 antall INT,
 PRIMARY KEY (kundeNR, filmID),
 FOREIGN KEY (kundeNR) REFERENCES kunde (kundeNR),
 FOREIGN KEY (filmID) REFERENCES film (filmID)
);


= ARK2500-H24 - Digital dokumentasjonsforvaltning

*_Mappeeksamen_*

_Sist oppdatert 2024-08-08_

Vær oppmerksom på at

* Eksamen består av tre deler og alle deler må besvares.
* Spørsmål 3 har et omfang på 6 900 tegn til 11.500 tegn. Dette inkluderer ikke en eventuell litteraturliste eller innholdsfortegnelse. Det inkluderer mellomrom.
* _Wikipedia er ikke en god referansekilde!_ Unngå å bruke wikipedia som referansekilde. Wikipedia vil ofte referere til en god referansekilde, bruk disse referansene i stedet.


<<<
== *_Spørsmål 1 (ER-modellering og SQL)_*

Ronny, naboen din, har nettopp fått en sprø idé som får smaksløkene til å danse! Han har spurt deg om du kan ta en titt på datamodelleringsbehovene for sitt nye prosjekt, et selskap som skal selge de mest fantastiske blandingene av iskrem. Ronny er ikke bare en iskremelsker; han er en ekte iskrem-entusiast som ønsker å dele denne deilige lidenskapen med verden.

Tenk deg å kjøpe inn de mest fantastiske typene iskrem og smakstilsetninger, blande dem sammen og skape kreasjoner som får folk til å smile fra øre til øre! Selskapet heter «_Scoopilicous_», og etter en del hyggelige diskusjoner har Ronny sendt deg en beskrivelse som han mener kan være et supert utgangspunkt for en ER-modell. Dette prosjektet handler om smak og glede, og det er klart at Ronny er klar for å ta iskremverdenen med storm! Ronny har sendt deg følgende informasjon om prosjektet:

_«Scoopilicous» ser en god forretningsmulighet i å kjøpe basisblandinger fra isgrossisten «Kremtastic is og kjeks» og komponere egne blandinger med forskjellige samkstilsetninger. «Kremtastic is og kjeks» leverer følgende basisbladninger:_

 * _Kremis med sukker_
 * _Kremis uten sukker_  
 * _Sjokolade kremis_
 * _Kremis med karamell_
 * _Sorbet med sukker_
 * _Sorbet uten sukker_

_Det er inngått avtaler med forskjellige leverandører av smakstilsetninger både fra Norge og utlandet. Fra Kina skal vi få en fantastisk smakstilsetning som er utviklet med et utgangspunkt i honning, men som har en litt bitter ettersmak som heter «Winne the xooh». Fra Sverige kjøper vil en spesiell smakstilsetning som heter «Sitronblomst» er laget fra hylleblomst og sitron som hadde fungert veldig godt til å lage en sorbet. Fra en leverandør i Torjulvågen får vi en litt kraftig kar som heter «Muggzilla» som kan blandes sammen med en karamellis som får smaksløkene til å danse tango. Vi har utviklet følgende iskremblandinger:_

 * _KREMSTAD_: 1Kg Kremis uten sukker
 * _SÖTSTOL_: 1Kg Kremis med karamell, 100g opphakket daimbiter
 * _FROSTIG_: 4000g Kremis med karamell, 1Kg opphakket karamellbiter
 * _KULKAR_: 15dl Kremis med karamell, 500g «Muggzilla» 
 * _ISBLOMMA_ : 4dl basisblanding av Sorbet uten sukker, 1dl «Sitronblomst»
 * _KULFEST_: 1Kg Sjokolade kremis, 54g Trinidad Moruga Scorpion
 * _FIKA_: 2Kg Kremis uten sukker, 1Kg ferske fiken

_Det er viktig at det skal være mulig å registrere andre blandinger senere. Ronny skal kjøpe inn en Tesla «Kreminator 3000», den mest intelligente iskremroboten som finnes. Denne kan blande is uansett hvilke enhet som brukes._

_EU direktivet om bærekraftig isproduksjon krever at hver is er sporbar 
tilbake til leverandørene jeg kjøper innholdet fra. Det er viktig at jeg 
holder oversikt over hvem som kjøper hvilke is slik at jeg kan ta 
kontakt med kunder dersom det er kvalitetsproblemer eller jeg må iverksette 
en tilbakekalling._

_Faktureringsprosessen vil bli håndtert av Odd Kåre som bor på den andre
siden av byen. Han trenger bare å vite hvem som kjøper hva og når. Kvaliteten 
er så bra at det ikke blir noe retur eller klager. Dersom det skulle forekomme 
tar jeg det manuelt._

=== Oppgaver

==== 1.1 ER-diagram

Lag et ER-diagram for databasen og forklar resonnementet bak løsningen
din. Her må du identifisere minst fire entiteter som du mener er relevant i ER-modellen for «_Scoopilicous_». For hver entitet, spesifiser attributter du mener er relevante. Det er også viktig å forklare sammenhengene mellom entitene.

==== 1.2 Logisk skjema

Lag det logiske skjemaet for databasen.

==== 1.3 Normalisering

Forklar hvordan du sikrer at databasens design er normalisert til minst 3NF. Hvordan eliminerer vi redundans og sikrer dataintegritet i denne prossesen?

==== 1.4 DDL

Lag et sett av DDL-kommandoer ut ifra det logiske skjemaet.

==== 1.5 DML

Lag et sett av DML-kommandoer som viser hvordan data settes inn i hver tabell. Det skal helst være maks 5 tupler per tabell men det er greit med flere om det er nødvendig for å svare på SQL spørsmålet.

==== 1.6 SQL

Lag en SQL-spørring som viser innholdet og mengdeforhold av ingredienser i hver isblanding

==== 1.7 SQL (JOIN)

Lag en eller flere SQL-spørringer som hvem som har kjøpt en isblanding som inneholder ingredienser levert fra en bestemt leverandør

=== Karaktersetting

Dersom du finner noe uklart i beskrivelsen, skriver du en kommentar
om eventuelle forutsetninger du har gjort. Når du lager det logiske
skjemaet, skal primærnøkler understrekes, mens fremmednøkler merkes med
en asterisk (*). Spørsmål 1.1 til 1.3 leveres som en PDF/A fil, mens spørsmål 1.4 til 1.7 leveres i en tekstfil med filendelse «.sql». Komandoene i spørsmål 1.4 til 1.7 må kunne kjøres sammenhengende uten feil. SQL filen skal være kommentert for å angi intensjon i arbeidet. Spørsmålene har følgende karakterfordeling:
 
 * 1.1 40%, det legges mest vekt på selvstendighet i resonementet
 * 1.2 5%
 * 1.3 20%
 * 1.4 10%
 * 1.5 10%
 * 1.6 5%
 * 1.7 10%


<<<
== *_Spørsmål 2 (Noark i database)_*

Oppgaven består i å utforske Noark 5 i en database og kunne utvikle DDL,
DML og SQL database kommandoer. Du skal lage en Noark 5 database
struktur som reflekterer arkivstrukturen i Vedlegg 1.

Besvarelsen vil bestå av følgende database kommandoer:

* CREATE DATABASE (DDL)
* CREATE TABLE (DDL)
* INSERT (DML)
* UPDATE (DML)
* SELECT (SQL)

For *CREATE* delen av oppgaven bruker du beskrivelsen i Vedlegg 1 og
Noark 5 metadatakatalogene. For *INSERT* delen av oppgaven skal
databasen inneholde følgende:

[cols=",",]
|===
|_arkivenhet_ |_# kommandoer_
|En arkiv |1
|En arkivskaper |1
|En arkivdel |1
|En klassifikasjonsystem |1
|Tre klasse |3
|En saksmappe per klasse |3
|To journalposter (Et inngående og utgående dokument) og en arkivnotat per saksmappe |9
|En korrespondansepart per journalpost | 6
|En dokumentbeskrivelse per journalpost / arkivnotat | 9
|En dokumentobjekt per dokumentbeskrivelse |9
|===

*Tabell 1*. _Strukturen som skal bygges_

For *UPDATE* delen av oppgaven skal du lage en SQL kommando som

* setter verdien av _saksstatus_ til «Utgår» dersom saken har ingen journalposter

For *SELECT* delen av oppgaven skal du lage SQL spørringer som kan gi en
generell oversikt over:

* hvor mange duplikat dokumenter det finnes i databasen (se sjekksum)

*Tilleggsinformasjon*.

* Dette er et rent elektronisk arkiv så M301 oppbevaringssted skal ikke være med. referanseArkivdel kan ansees å være 0-1, ikke 0-M for mappe og registrering
* virksomhetsspesifikkeMetadata er ikke relevant i dette spørsmålet og skal ikke være med
* Besvarelsen består av en tekstfil som inneholder alle kommandoer. Alle kommandoer i denne tekstfilen må kunne kjøres feilfritt etter hverandre
* Attributtnavn skal følge Noark 5 metadatakatalogen. Dette gjelder også
bruken av små og store bokstaver. Metadatakatalogen kan være nyttig til å forstå metadataelementene for å komme fram til fornuftige verdier
* Noark 5v5 skal brukes som grunnlag for oppgaven
* Det elektroniske dokumentet antaes å være lagret i en mappe som heter "dokumenter".
* Du trenger ikke å lage de elektroniske dokumenter som referanseDokumentFil refererer til. Det holder med at referanseDokumentFil har en verdi som peker til et fiktiv dokument
* systemID skal være basert på UUID
* Arkivenheten arkivskaper skal ha en systemID attributt


<<<
== *_Spørsmål 3_*

Drøft påstanden «En sentralisert arkivstruktur i henhold til Noark 5 løser dagens arkivutfordringer»

Kandidaten står fritt til å svare spørsmålet i den retningen de selv ønsker, med de kildene og refleksjoner kandidaten selv mener er relevant. Det er mulig å være helt eller delvis enig/uenig med påstanden. 

<<<
== _Vedlegg 1: Arkivstruktur_
image:media/struktur.svg[Arkivstruktur,scaledwidth=80%,align="center"]

_Figur 1: Konseptuell databasestruktur til spørsmål 2_

<<<
== _Vedlegg 2: Informasjon om innlevering._ 

Hele mappen leveres som én zip-fil. 

*Spørsmål 1* leveres som to filer.

Del 1 – 3 leveres som en PDF/A fil. Filen skal hete
«*spm1_1_til_3.pdf*».

Del 4 – 7 leveres som en tekst fil med filendelse «.sql». Filen skal
hete «*spm1_4_til_7.sql*».

*Spørsmål 2* leveres som en SQL-fil.

Filen skal hete «*spm2.sql*»

*Spørsmål 3* leveres som en PDF/A fil.

Filen skal hete «*spm3.pdf*».

Alle filene skal være i samme mappe, dvs du trenger ikke å lage
undermapper for hver spørsmål. Hvis du ikke vet hvordan du lager en
zip-fil, er det viktig å snakke med faglærer i god tid før
innleveringsfristen. Navnet på filen du skal laste opp vil være en
kombinasjon av eksamensnummeret ditt (ikke studentnummer) og kurskode
(ARK2500). Følgende er et eksempel filnavn:

*ark2500_h24_kan_112.zip*

image:media/image1.png[Zipfil,width=253,height=219,align="center"]

_Figur 2: Innholdet i zip-filen_
